// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require turbolinks
//= require jquery.turbolinks
//= require bootstrap
//= require_tree .

function changeBackgroundColor(k, mouseOver){
  var x = document.getElementsByTagName("td");
  var rowIndex = (k.parentNode.rowIndex - 1) * 2;
  var color;
  if(!("" + mouseOver).localeCompare("0")){
    color = "red";
  }else{
    if(k.parentNode.rowIndex % 2 == 0){
      color = "white";
    }else{
      color = "green";
    }
  }
  x[rowIndex].style.backgroundColor = color;
  x[rowIndex + 1].style.backgroundColor = color;
}

function copyAmount(element){
  if(element.id == "journal_from_amount"){
    document.getElementById("journal_to_amount").value = element.value;
  }else{
    document.getElementById("journal_from_amount").value = element.value;
  }
}

$(function(){
	$(window).load(function(){ // On load
		$('#main').css('margin-top', '' + $(".container").height()+'px');
	});
	$(window).resize(function(){ // On resize
		$('#main').css('margin-top', '' + $(".container").height()+'px');
	});
	
	var value_konto_nummer_kontenplan = [{
        label: "Ausstehende Einlagen",
        value: "0000"},
    {
        label: "Frei",
        value: "01"},
    {
        label: "Konzessionen",
        value: "0200"},
    {
        label: "Geschäfts- oder Firmenwert",
        value: "0300"},
    {
        label: "Frei",
        value: "04"},
    {
        label: "Unbebaute Grundstücke",
        value: "0500"},
    {
        label: "Bebaute Grundstücke",
        value: "0510"},
    {
        label: "Gebäude (Sammelkonto)",
        value: "0520"},
    {
        label: "Betriebsgebäude",
        value: "0530"},
    {
        label: "Verwaltungsgebäude",
        value: "0540"},
    {
        label: "Andere Bauten",
        value: "0550"},
    {
        label: "Grundstückseinrichtungen",
        value: "0560"},
    {
        label: "Gebäudeeinrichtungen",
        value: "0570"},
    {
        label: "Wohngebäude",
        value: "0590"},
    {
        label: "Frei",
        value: "06"},
    {
        label: "Anlagen und Maschinen der Energieversorgung",
        value: "0700"},
    {
        label: "Anlagen der Materiallagerung und -bereitstellung",
        value: "0710"},
    {
        label: "Anlagen und Maschinen der mechanischen Materialbearbeitung, -verarbeitung und -umwandlung",
        value: "0720"},
    {
        label: "Anlagen für Wärme-, Kälte und chemische Prozesse sowie ähnliche Anlagen",
        value: "0730"},
    {
        label: "Anlagen für Arbeitssicherheit und Umweltschutz",
        value: "0740"},
    {
        label: "Transportanlagen und ähnliche Betriebsvorrichtungen",
        value: "0750"},
    {
        label: "Verpackungsanlagen und -maschinen",
        value: "0760"},
    {
        label: "Sonstige Anlagen und Maschinen",
        value: "0770"},
    {
        label: "Reservemaschinen und -anlagenteile",
        value: "0780"},
    {
        label: "Geringwertige Anlagen und Maschinen",
        value: "0790"},
    {
        label: "GWG Sammelposten Anlagen und Maschinen Jahr 1",
        value: "0791"},
    {
        label: "GWG Sammelposten Anlagen und Maschinen Jahr 5",
        value: "0795"},
    {
        label: "Andere Anlagen, Betriebs- und Geschäftsausstattung",
        value: "08"},
    {
        label: "Andere Anlagen",
        value: "0800"},
    {
        label: "Werkstätteneinrichtung",
        value: "0810"},
    {
        label: "Werkzeuge, Werksgeräte und Modelle, Prüf- und Messmittel",
        value: "0820"},
    {
        label: "Lager- u. Transporteinrichtungen",
        value: "0830"},
    {
        label: "Fuhrpark",
        value: "0840"},
    {
        label: "Sonstige Betriebsausstattung",
        value: "0850"},
    {
        label: "Büromaschinen, Organisationsmit-tel und Kommunikationsanlagen",
        value: "0860"},
    {
        label: "Büromöbel und sonstige Geschäftsausstattung",
        value: "0870"},
    {
        label: "Reserveteile für Betriebs- und Geschäftsausstattung",
        value: "0880"},
    {
        label: "Geringwertige Vermögensgegenstände der Betriebsund Geschäftsausstattung",
        value: "0890"},
    {
        label: "GWG-Sammelposten BGA Jahr 1",
        value: "0891"},
    {
        label: "GWG-Sammelposten BGA Jahr 5",
        value: "0895"},
    {
        label: "Geleistete Anzahlungen und Anlagen im Bau",
        value: "09"},
    {
        label: "Geleistete Anzahlungen auf Sachanlagen",
        value: "0900"},
    {
        label: "Anlagen im Bau",
        value: "0950"},
    {
        label: "Frei",
        value: "10"},
    {
        label: "Frei",
        value: "11"},
    {
        label: "Frei",
        value: "12"},
    {
        label: "Beteiligungen",
        value: "13"},
    {
        label: "Beteiligungen",
        value: "1300"},
    {
        label: "Frei",
        value: "14"},
    {
        label: "Wertpapiere d. Anlagevermögens",
        value: "15"},
    {
        label: "Wertpapiere d. Anlagevermögens",
        value: "1500"},
    {
        label: "Sonstige Finanzanlagen",
        value: "16"},
    {
        label: "Sonstige Finanzanlagen",
        value: "1600"},
    {
        label: "Frei",
        value: "17"},
    {
        label: "Frei",
        value: "18"},
    {
        label: "Frei",
        value: "19"},
    {
        label: "Roh-, Hilfs- und Betriebsstoffe e",
        value: "20"},
    {
        label: "Rohstoffe/Fertigungsmaterial",
        value: "2000"},
    {
        label: "Bezugskosten",
        value: "2001"},
    {
        label: "Nachlässe",
        value: "2002"},
    {
        label: "Vorprodukte/Fremdbauteile",
        value: "2010"},
    {
        label: "Bezugskosten",
        value: "2011"},
    {
        label: "Nachlässe",
        value: "2012"},
    {
        label: "Hilfsstoffe",
        value: "2020"},
    {
        label: "Bezugskosten",
        value: "2021"},
    {
        label: "Nachlässe",
        value: "2022"},
    {
        label: "Betriebsstoffe",
        value: "2030"},
    {
        label: "Bezugskosten",
        value: "2031"},
    {
        label: "Nachlässe",
        value: "2032"},
    {
        label: "Sonstiges Material",
        value: "2070"},
    {
        label: "Bezugskosten",
        value: "2071"},
    {
        label: "Nachlässe",
        value: "2072"},
    {
        label: "Unfertige Erzeugnisse, unfertige Leistungen",
        value: "21"},
    {
        label: "Unfertige Erzeugnisse",
        value: "2100"},
    {
        label: "Unfertige Leistungen",
        value: "2190"},
    {
        label: "Fertige Erzeugnisse und Waren",
        value: "22"},
    {
        label: "Fertige Erzeugnisse",
        value: "2200"},
    {
        label: "Waren (Handelswaren) e",
        value: "2280"},
    {
        label: "Bezugskosten",
        value: "2281"},
    {
        label: "Nachlässe",
        value: "2282"},
    {
        label: "Geleistete Anzahlungen a. Vorräte",
        value: "23"},
    {
        label: "Geleistete Anzahlungen a. Vorräte",
        value: "2300"},
    {
        label: "Ford. a. Lieferungen u. Leistungen",
        value: "24"},
    {
        label: "Forderungen aus Lieferungen und Leistungen",
        value: "2400"},
    {
        label: "Kaufpreisforderungen",
        value: "2420"},
    {
        label: "Umsatzsteuerforderungen",
        value: "2421"},
    {
        label: "Wechselford. aus Lieferungen und Leistungen (Besitzwechsel)",
        value: "2450"},
    {
        label: "Zweifelhafte Forderungen",
        value: "2470"},
    {
        label: "Protestwechsel",
        value: "2480"},
    {
        label: "Innergemeinschaftlicher Erwerb/Einfuhr",
        value: "25"},
    {
        label: "Innergemeinschaftl. Erwerb",
        value: "2500"},
    {
        label: "Bezugskosten",
        value: "2501"},
    {
        label: "Nachlässe",
        value: "2502"},
    {
        label: "Gütereinfuhr",
        value: "2510"},
    {
        label: "Bezugskosten",
        value: "2511"},
    {
        label: "Nachlässe",
        value: "2512"},
    {
        label: "Sonstige Vermögensgegenstände",
        value: "26"},
    {
        label: "Vorsteuer",
        value: "2600"},
    {
        label: "Vorsteuer (19 %) für i. E.",
        value: "2602"},
    {
        label: "Einfuhrumsatzsteuer",
        value: "2604"},
    {
        label: "Sonst. Ford. an Finanzbehörden",
        value: "2630"},
    {
        label: "SV-Vorauszahlung",
        value: "2640"},
    {
        label: "Forderungen an Mitarbeiter",
        value: "2650"},
    {
        label: "Übrige sonstige Forderungen",
        value: "2690"},
    {
        label: "Wertpapiere des Umlaufvermögens",
        value: "27"},
    {
        label: "Wertpapiere des Umlaufvermögens",
        value: "2700"},
    {
        label: "Flüssige Mittel",
        value: "28"},
    {
        label: "Guthaben bei Kreditinstituten (Bank)",
        value: "2800"},
    {
        label: "Postbank",
        value: "2850"},
    {
        label: "Schecks",
        value: "2860"},
    {
        label: "Bundesbank",
        value: "2870"},
    {
        label: "Kasse",
        value: "2880"},
    {
        label: "Nebenkassen",
        value: "2890"},
    {
        label: "Aktive Rechnungsabgrenzung (und Bilanzfehlbetrag)",
        value: "29"},
    {
        label: "Aktive Jahresabgrenzung",
        value: "2900"},
    {
        label: "Umsatzsteuer auf erhaltene Anzahlungen",
        value: "2920"},
    {
        label: "Disagio",
        value: "2930"},
    {
        label: "(nicht durch Eigenkapital gedeckter Fehlbetrag)",
        value: "2990"},
    {
        label: "Eigenkapital/ Gezeichnetes Kapital",
        value: "30"},
    {
        label: "Eigenkapital",
        value: "3000"},
    {
        label: "Privatkonto Bei Personengesellschaften",
        value: "3001"},
    {
        label: "Kapital Gesellschafter A",
        value: "3000"},
    {
        label: "Privatkonto A",
        value: "3001"},
    {
        label: "Kapital Gesellschafter B",
        value: "3010"},
    {
        label: "Privatkonto B",
        value: "3011"},
    {
        label: "Kommanditkapital Gesellschafter C",
        value: "3070"},
    {
        label: "Kommanditkapital Gesellschafter D",
        value: "3080"},
    {
        label: "Gezeichnetes Kapital (Grundkapital/Stammkapital)",
        value: "3000"},
    {
        label: "Kapitalrücklage",
        value: "31"},
    {
        label: "Kapitalrücklage",
        value: "3100"},
    {
        label: "Gewinnrücklagen",
        value: "32"},
    {
        label: "Gesetzliche Rücklagen",
        value: "3210"},
    {
        label: "Satzungsmäßige Rücklagen",
        value: "3230"},
    {
        label: "Andere Gewinnrücklagen",
        value: "3240"},
    {
        label: "Ergebnisverwendung b",
        value: "33"},
    {
        label: "Jahresergebnis des Vorjahres",
        value: "3310"},
    {
        label: "Ergebnisvortrag aus früheren Perioden",
        value: "3320"},
    {
        label: "Veränderung der Rücklagen",
        value: "3340"},
    {
        label: "Bilanzgewinn/Bilanzverlust",
        value: "3350"},
    {
        label: "Ergebnisausschüttung",
        value: "3360"},
    {
        label: "Ergebnisvortrag auf neue Rechnung",
        value: "3390"},
    {
        label: "Jahresüberschuss/ Jahresfehlbetrag",
        value: "34"},
    {
        label: "Jahresüberschuss/ Jahresfehlbetrag",
        value: "3400"},
    {
        label: "Sonderposten mit Rücklageanteil",
        value: "35"},
    {
        label: "Sonderposten mit Rücklageanteil",
        value: "3500"},
    {
        label: "Wertberichtigungen (Bei Kapitalgesellschaften als Passivposten der Bilanz nicht mehr zulässig)",
        value: "36"},
    {
        label: "zu Sachanlagen",
        value: "3610"},
    {
        label: "zu Finanzanlagen",
        value: "3650"},
    {
        label: "Einzelwertberichtigung zu Forderungen",
        value: "3670"},
    {
        label: "Pauschalwertberichtigung zu Forderungen Rückstellungen",
        value: "3680"},
    {
        label: "Rückstellungen für Pensionen und ähnliche Verpflichtungen",
        value: "37"},
    {
        label: "Rückstellungen für Pensionen und ähnliche Verpflichtungen",
        value: "3700"},
    {
        label: "Steuerrückstellungen",
        value: "38"},
    {
        label: "Steuerrückstellungen",
        value: "3800"},
    {
        label: "Sonstige Rückstellungen c",
        value: "39"},
    {
        label: "für Gewährleistung",
        value: "3910"},
    {
        label: "für andere ungewisse Verbindlichkeiten",
        value: "3930"},
    {
        label: "für drohende Verluste aus schwebenden Geschäften",
        value: "3970"},
    {
        label: "für Aufwendungen",
        value: "3990"},
    {
        label: "Frei",
        value: "40"},
    {
        label: "Anleihen",
        value: "41"},
    {
        label: "Anleihen",
        value: "4100"},
    {
        label: "Verbindlichkeiten gegenüber Kreditinstituten",
        value: "42"},
    {
        label: "Kurzfristige Bankverbindlichkeiten",
        value: "4210"},
    {
        label: "Mittelfristige Bankverbindlichkeiten",
        value: "4230"},
    {
        label: "Langfristige Bankverbindlichkeiten",
        value: "4250"},
    {
        label: "Erhaltene Anzahlungen auf Bestellungen",
        value: "43"},
    {
        label: "Erhaltene Anzahlungen",
        value: "4300"},
    {
        label: "Verbindlichkeiten aus Lieferungen und Leistungen",
        value: "44"},
    {
        label: "Verbindlichkeiten aus Lieferungen und Leistungen",
        value: "4400"},
    {
        label: "Kaufpreisverbindlichkeiten",
        value: "4420"},
    {
        label: "Wechselverbindlichkeiten",
        value: "45"},
    {
        label: "Schuldwechsel",
        value: "4500"},
    {
        label: "Frei",
        value: "46"},
    {
        label: "Frei",
        value: "47"},
    {
        label: "Sonstige Verbindlichkeiten",
        value: "48"},
    {
        label: "Umsatzsteuer",
        value: "4800"},
    {
        label: "Umsatzsteuer (19 %) für i. E.",
        value: "4802"},
    {
        label: "Zollverbindlichkeiten",
        value: "4820"},
    {
        label: "Sonstige Verbindlichkeiten gegenüber Finanzbehörden",
        value: "4830"},
    {
        label: "Verbindlichkeiten gegenüber Mitarbeitern",
        value: "4850"},
    {
        label: "Verbindlichkeiten aus vermö- genswirksamen Leistungen",
        value: "4860"},
    {
        label: "Verbindlichkeiten gegenüber Gesellschaftern (Dividende)",
        value: "4870"},
    {
        label: "Übrige sonstige Verbindlichkeiten",
        value: "4890"},
    {
        label: "Passive Rechnungsabgrenzung",
        value: "49"},
    {
        label: "Passive Jahresabgrenzung",
        value: "4900"},
    {
        label: "Umsatzerlöse für eigene Erzeugnisse u. andere eigene Leistungen",
        value: "50"},
    {
        label: "Umsatzerlöse f. eigene Erzeugn.",
        value: "5000"},
    {
        label: "Erlösberichtigungen",
        value: "5001"},
    {
        label: "Umsatzerlöse für andere eigene Leistungen",
        value: "5050"},
    {
        label: "Erlösberichtigungen",
        value: "5051"},
    {
        label: "Erlöse aus innergemeinschaftlicher Lieferung (i. L.)",
        value: "5060"},
    {
        label: "Erlösberichtigungen",
        value: "5061"},
    {
        label: "Erlöse aus Güterausfuhr",
        value: "5070"},
    {
        label: "Erlösberichtigungen",
        value: "5071"},
    {
        label: "Umsatzerlöse für Waren und sonstige Umsatzerlöse",
        value: "51"},
    {
        label: "Umsatzerlöse für Waren",
        value: "5100"},
    {
        label: "Erlösberichtigungen",
        value: "5101"},
    {
        label: "Sonstige Umsatzerlöse",
        value: "5190"},
    {
        label: "Erlösberichtigungen",
        value: "5191"},
    {
        label: "Erhöhung oder Verminderung des Bestandes an unfertigen und fertigen Erzeugnissen",
        value: "52"},
    {
        label: "Bestandsveränderungen",
        value: "5200"},
    {
        label: "Bestandsveränderungen an unfertigen Erzeugnissen und nicht abgerechneten Leistungen",
        value: "5201"},
    {
        label: "Bestandsveränderungen an fertigen Erzeugnissen",
        value: "5202"},
    {
        label: "Andere aktivierte Eigenleistungen",
        value: "53"},
    {
        label: "Aktivierte Eigenleistungen",
        value: "5300"},
    {
        label: "Sonstige betriebliche Erträge",
        value: "54"},
    {
        label: "Mieterträge",
        value: "5400"},
    {
        label: "Leasingerträge",
        value: "5401"},
    {
        label: "Sonstige Erlöse (z. B. aus Provisionen oder Anlagenabgängen)",
        value: "5410"},
    {
        label: "Entnahme von Gegenständen und sonstigen Leistungen",
        value: "5420"},
    {
        label: "Andere sonstige betriebl. Erträge",
        value: "5430"},
    {
        label: "Erträge aus Werterhöhungen von Gegenständen des Anlage-vermögens(Zuschreibungen)",
        value: "5440"},
    {
        label: "Erträge aus Zuschreibungen zum Umlaufvermögen",
        value: "5441"},
    {
        label: "Erträge aus der Auflösung oder Herabsetzung von Wertberichtigungen auf Forderungen",
        value: "5450"},
    {
        label: "Erträge aus der Herabsetzung von Rückstellungen",
        value: "5480"},
    {
        label: "Periodenfremde Erträge",
        value: "5490"},
    {
        label: "Erträge aus Beteiligungen",
        value: "55"},
    {
        label: "Erträge aus Beteiligungen",
        value: "5500"},
    {
        label: "Erträge aus anderen Wertpapieren und Ausleihungen des Finanzanlagevermögens",
        value: "56"},
    {
        label: "Erträge aus anderen Finanzanlagen",
        value: "5600"},
    {
        label: "Sonstige Zinsen und ähnliche Erträge",
        value: "57"},
    {
        label: "Zinserträge",
        value: "5710"},
    {
        label: "Diskonterträge",
        value: "5730"},
    {
        label: "Erträge aus Wertpapieren des Umlaufvermögens",
        value: "5780"},
    {
        label: "Sonstige zinsähnliche Erträge",
        value: "5790"},
    {
        label: "Außerordentliche Erträge",
        value: "58"},
    {
        label: "Außerordentliche Erträge",
        value: "5800"},
    {
        label: "Frei",
        value: "59"},
    {
        label: "Aufwendungen für Roh-, Hilfs-und Betriebsstoffe und für bezogene Waren e",
        value: "60"},
    {
        label: "Aufwendungen für Rohstoffe/Fertigungsmaterial",
        value: "6000"},
    {
        label: "Bezugskosten",
        value: "6001"},
    {
        label: "Nachlässe",
        value: "6002"},
    {
        label: "Aufwendungen für Vorprodukte/Fremdbauteile e",
        value: "6010"},
    {
        label: "Aufwendungen für Hilfsstoffe e",
        value: "6020"},
    {
        label: "Aufwendungen für Betriebsstoffe/Verbrauchswerkzeuge e",
        value: "6030"},
    {
        label: "Aufw. für Verpackungsmaterial",
        value: "6040"},
    {
        label: "Aufw. für Energie u. Treibstoffe",
        value: "6050"},
    {
        label: "Aufw. für Reparaturmaterial",
        value: "6060"},
    {
        label: "Aufwendungen für sonstiges Material",
        value: "6070"},
    {
        label: "Aufwendungen für Waren e",
        value: "6080"},
    {
        label: "Aufwendungen für bezogene Leistungen",
        value: "61"},
    {
        label: "Fremdleistungen für Erzeugnisse und andere Umsatzleistungen",
        value: "6100"},
    {
        label: "Frachten und Fremdlager",
        value: "6140"},
    {
        label: "Vertriebsprovisionen",
        value: "6150"},
    {
        label: "Fremdinstandhaltung",
        value: "6160"},
    {
        label: "Sonstige Aufwendungen für bezogene Leistungen",
        value: "6170"},
    {
        label: "Löhne",
        value: "62"},
    {
        label: "Löhne einschl. tariflicher, vertraglicher oder arbeitsbedingter Zulagen",
        value: "6200"},
    {
        label: "Urlaubs- und Weihnachtsgeld",
        value: "6210"},
    {
        label: "Sonstige tarifliche oder vertragliche Aufwendungen für Lohnempfänger",
        value: "6220"},
    {
        label: "Freiwillige Zuwendungen",
        value: "6230"},
    {
        label: "Sachbezüge",
        value: "6250"},
    {
        label: "Vergütungen an gewerbliche Auszubildende",
        value: "6260"},
    {
        label: "Gehälter",
        value: "63"},
    {
        label: "Gehälter und Zulagen",
        value: "6300"},
    {
        label: "Urlaubs- und Weihnachtsgeld",
        value: "6310"},
    {
        label: "Sonstige tarifliche oder vertragliche Aufwendungen",
        value: "6320"},
    {
        label: "Freiwillige Zuwendungen",
        value: "6330"},
    {
        label: "Sachbezüge",
        value: "6350"},
    {
        label: "Vergütungen an Auszubildende",
        value: "6360"},
    {
        label: "Soziale Abgaben und Aufwendungen für Altersversorgung und für Unterstützung",
        value: "64"},
    {
        label: "Arbeitgeberanteil zur Sozialversicherung (Lohnbereich) d",
        value: "6400"},
    {
        label: "Arbeitgeberanteil zur Sozialversicherung (Gehaltsbereich) d",
        value: "6410"},
    {
        label: "Beiträge zur Berufsgenossenschaft",
        value: "6420"},
    {
        label: "Aufwendungen für Altersversorgung",
        value: "6440"},
    {
        label: "Aufwendungen für Unterstützung",
        value: "6490"},
    {
        label: "Sonstige soziale Aufwendungen",
        value: "6495"},
    {
        label: "Abschreibungen auf Anlagevermögen",
        value: "65"},
    {
        label: "Abschreibungen auf immaterielle Vermögensgegenstände des Anlagevermögens",
        value: "6510"},
    {
        label: "Abschreibungen auf Sachanlagen",
        value: "6520"},
    {
        label: "Abschreibungen auf geringwertige Wirtschaftsgüter",
        value: "6540"},
    {
        label: "Abschreibungen auf GWG-Sammelposten Jahr 1",
        value: "6541"},
    {
        label: "Abschreibungen auf GWG-Sammelposten Jahr 5",
        value: "6545"},
    {
        label: "Außerplanmäßige Abschreibungen auf Sachanlagen",
        value: "6550"},
    {
        label: "Unüblich hohe Abschreibungen auf Umlaufvermögen",
        value: "6570"},
    {
        label: "Sonstige Personalaufwendungen",
        value: "66"},
    {
        label: "Aufwendungen für Personaleinstellung",
        value: "6600"},
    {
        label: "Aufwendungen für übernommene Fahrtkosten",
        value: "6610"},
    {
        label: "Aufwendungen für Werksarzt und Arbeitssicherheit",
        value: "6620"},
    {
        label: "Personenbezogene Versicherungen",
        value: "6630"},
    {
        label: "Aufwendungen für Fort- und Weiterbildung",
        value: "6640"},
    {
        label: "Aufwendungen für Dienstjubiläen",
        value: "6650"},
    {
        label: "Aufwendungen für Belegschaftsveranstaltungen",
        value: "6660"},
    {
        label: "Aufwendungen für Werksküche und Sozialeinrichtungen",
        value: "6670"},
    {
        label: "Ausgleichsabgabe nach dem Schwerbehindertengesetz",
        value: "6680"},
    {
        label: "Übrige sonstige Personalaufwendungen",
        value: "6690"},
    {
        label: "Aufwendungen für die Inanspruchnahme von Rechten und Diensten",
        value: "67"},
    {
        label: "Mieten, Pachten",
        value: "6700"},
    {
        label: "Leasingaufwendungen",
        value: "6710"},
    {
        label: "Lizenzen und Konzessionen",
        value: "6720"},
    {
        label: "Gebühren",
        value: "6730"},
    {
        label: "Kosten des Geldverkehrs",
        value: "6750"},
    {
        label: "Provisionsaufwendungen (außer Vertriebsprovisionen)",
        value: "6760"},
    {
        label: "Rechts- und Beratungskosten",
        value: "6770"},
    {
        label: "Aufwendungen für Kommunikation(Dokumentation, Information, Reisen, Werbung)",
        value: "68"},
    {
        label: "Büromaterial",
        value: "6800"},
    {
        label: "Zeitungen und Fachliteratur",
        value: "6810"},
    {
        label: "Portokosten",
        value: "6820"},
    {
        label: "Kosten der Telekommunikation",
        value: "6830"},
    {
        label: "Reisekosten",
        value: "6850"},
    {
        label: "Bewirtung und Präsentation",
        value: "6860"},
    {
        label: "Werbung",
        value: "6870"},
    {
        label: "Spenden",
        value: "6880"},
    {
        label: "Aufwendungen für Beiträge und Sonstiges sowie Wertkorrekturen und periodenfremde Aufwendungen",
        value: "69"},
    {
        label: "Versicherungsbeiträge",
        value: "6900"},
    {
        label: "Beiträge zu Wirtschaftsverbänden und Berufsvertretungen",
        value: "6920"},
    {
        label: "Verluste aus Schadensfällen",
        value: "6930"},
    {
        label: "Sonstige Aufwendungen",
        value: "6940"},
    {
        label: "Abschreibungen auf Forderungen",
        value: "6950"},
    {
        label: "Abschreibungen auf Forderungen wegen Uneinbringlichkeit",
        value: "6951"},
    {
        label: "Einstellung in Einzelwertberichtigung",
        value: "6952"},
    {
        label: "Einstellung in Pauschalwertberichtigung",
        value: "6953"},
    {
        label: "Verluste aus dem Abgang von Vermögensgegenständen",
        value: "6960"},
    {
        label: "Anlagenabgänge",
        value: "6979"},
    {
        label: "Zuführungen zu Rückstellungen für Gewährleistung",
        value: "6980"},
    {
        label: "Periodenfremde Aufwendungen",
        value: "6990"},
    {
        label: "Betriebliche Steuern",
        value: "70"},
    {
        label: "Grundsteuer",
        value: "7020"},
    {
        label: "Grundsteuer – Vorjahr",
        value: "7021"},
    {
        label: "Kraftfahrzeugsteuer",
        value: "7030"},
    {
        label: "Kraftfahrzeugsteuer – Vorjahr",
        value: "7031"},
    {
        label: "Steuerrückerstattung für KfzSteuer – Vorjahr",
        value: "7032"},
    {
        label: "Ausfuhrzölle",
        value: "7070"},
    {
        label: "Verbrauchsteuern",
        value: "7080"},
    {
        label: "Sonstige betriebliche Steuern",
        value: "7090"},
    {
        label: "Frei",
        value: "71"},
    {
        label: "Frei",
        value: "72"},
    {
        label: "Frei",
        value: "73"},
    {
        label: "Abschreibungen auf Finanzanlagen und auf Wertpapiere des Umlaufvermögens und Verluste aus entsprechenden Abgängen",
        value: "74"},
    {
        label: "Abschreibungen auf Finanzanlagen",
        value: "7400"},
    {
        label: "Abschreibungen auf Wertpapiere des Umlaufvermögens",
        value: "7420"},
    {
        label: "Verluste aus dem Abgang von Finanzanlagen",
        value: "7450"},
    {
        label: "Verluste aus dem Abgang von Wertpapieren des Umlaufvermögens",
        value: "7460"},
    {
        label: "Zinsen und ähnliche Aufwendungen",
        value: "75"},
    {
        label: "Zinsaufwendungen",
        value: "7510"},
    {
        label: "Diskontaufwendungen",
        value: "7530"},
    {
        label: "Sonstige zinsähnliche Aufwendungen",
        value: "7590"},
    {
        label: "Außerordentliche Aufwendungen",
        value: "76"},
    {
        label: "Außerordentliche Aufwendungen",
        value: "7600"},
    {
        label: "Steuern vom Einkommen und Ertrag",
        value: "77"},
    {
        label: "Gewerbesteuer",
        value: "7700"},
    {
        label: "Gewerbesteuer – Vorjahr",
        value: "7701"},
    {
        label: "Steuerrückerstattung für Gewerbesteuer – Vorjahr",
        value: "7702"},
    {
        label: "Körperschaftsteuer",
        value: "7710"},
    {
        label: "Kapitalertragsteuer",
        value: "7720"},
    {
        label: "Diverse Aufwendungen",
        value: "78"},
    {
        label: "Diverse Aufwendungen",
        value: "7800"},
    {
        label: "Frei",
        value: "79"},
    {
        label: "Eröffnung/Abschluss",
        value: "80"},
    {
        label: "Eröffnungsbilanzkonto",
        value: "8000"},
    {
        label: "Schlussbilanzkonto",
        value: "8010"},
    {
        label: "GuV-Konto Gesamtkostenverfahren",
        value: "8020"},
    {
        label: "GuV-Konto Umsatzkostenverfahren",
        value: "8030"},
    {
        label: "Saldenvorträge(Sammelkonto)Konten der Kostenbereiche für die GuV im Umsatzkostenverfahren",
        value: "8050"},
    {
        label: "Herstellungskosten",
        value: "81"},
    {
        label: "Vertriebskosten",
        value: "82"},
    {
        label: "Allgemeine Verwaltungskosten",
        value: "83"},
    {
        label: "Sonstige betriebliche Aufwendungen Konten der kurzfristigen Erfolgsrechnung (KER) für innerjährige Rechnungsperioden (Monat, Quartal oder Halbjahr)",
        value: "84"},
    {
        label: "Korrekturkonten zu den Erträgen der Kontenklasse 5",
        value: "85"},
    {
        label: "Korrekturkonten zu den Aufwendungen der Kontenklasse 6",
        value: "86"},
    {
        label: "Korrekturkonten zu den Aufwendungen der Kontenklasse 7",
        value: "87"},
    {
        label: "Kurzfristige Erfolgsrechnung (KER)",
        value: "88"},
    {
        label: "Gesamtkostenverfahren",
        value: "8800"},
    {
        label: "Umsatzkostenverfahren",
        value: "8810"},
    {
        label: "Innerjährige Rechnungsabgrenzung",
        value: "89"},
    {
        label: "Aktive Rechnungsabgrenzung",
        value: "8900"},
    {
        label: "Unternehmensbezogene Abgrenzungen (neutrale Aufwendungen u. Erträge)",
        value: "90"},
    {
        label: "Kostenrechnerische Korrekturen",
        value: "91"},
    {
        label: "Kostenarten und Leistungsarten",
        value: "92"},
    {
        label: "Kostenstellen",
        value: "93"},
    {
        label: "Kostenträger",
        value: "94"},
    {
        label: "Fertige Erzeugnisse",
        value: "95"},
    {
        label: "Interne Lieferungen und Leistungen sowie deren Kosten",
        value: "96"},
    {
        label: "Umsatzkosten",
        value: "97"},
    {
        label: "Umsatzleistungen",
        value: "98"},
    {
        label: "Ergebnisausweise",
        value: "99"}];

  var value_konto_name_kontenplan = [{
        label: "0000",
        value: "Ausstehende Einlagen"},
    {
        label: "01",
        value: "Frei"},
    {
        label: "0200",
        value: "Konzessionen"},
    {
        label: "0300",
        value: "Geschäfts- oder Firmenwert"},
    {
        label: "04",
        value: "Frei"},
    {
        label: "0500",
        value: "Unbebaute Grundstücke"},
    {
        label: "0510",
        value: "Bebaute Grundstücke"},
    {
        label: "0520",
        value: "Gebäude (Sammelkonto)"},
    {
        label: "0530",
        value: "Betriebsgebäude"},
    {
        label: "0540",
        value: "Verwaltungsgebäude"},
    {
        label: "0550",
        value: "Andere Bauten"},
    {
        label: "0560",
        value: "Grundstückseinrichtungen"},
    {
        label: "0570",
        value: "Gebäudeeinrichtungen"},
    {
        label: "0590",
        value: "Wohngebäude"},
    {
        label: "06",
        value: "Frei"},
    {
        label: "0700",
        value: "Anlagen und Maschinen der Energieversorgung"},
    {
        label: "0710",
        value: "Anlagen der Materiallagerung und -bereitstellung"},
    {
        label: "0720",
        value: "Anlagen und Maschinen der mechanischen Materialbearbeitung, -verarbeitung und -umwandlung"},
    {
        label: "0730",
        value: "Anlagen für Wärme-, Kälte und chemische Prozesse sowie ähnliche Anlagen"},
    {
        label: "0740",
        value: "Anlagen für Arbeitssicherheit und Umweltschutz"},
    {
        label: "0750",
        value: "Transportanlagen und ähnliche Betriebsvorrichtungen"},
    {
        label: "0760",
        value: "Verpackungsanlagen und -maschinen"},
    {
        label: "0770",
        value: "Sonstige Anlagen und Maschinen"},
    {
        label: "0780",
        value: "Reservemaschinen und -anlagenteile"},
    {
        label: "0790",
        value: "Geringwertige Anlagen und Maschinen"},
    {
        label: "0791",
        value: "GWG Sammelposten Anlagen und Maschinen Jahr 1"},
    {
        label: "0795",
        value: "GWG Sammelposten Anlagen und Maschinen Jahr 5"},
    {
        label: "08",
        value: "Andere Anlagen, Betriebs- und Geschäftsausstattung"},
    {
        label: "0800",
        value: "Andere Anlagen"},
    {
        label: "0810",
        value: "Werkstätteneinrichtung"},
    {
        label: "0820",
        value: "Werkzeuge, Werksgeräte und Modelle, Prüf- und Messmittel"},
    {
        label: "0830",
        value: "Lager- u. Transporteinrichtungen"},
    {
        label: "0840",
        value: "Fuhrpark"},
    {
        label: "0850",
        value: "Sonstige Betriebsausstattung"},
    {
        label: "0860",
        value: "Büromaschinen, Organisationsmit-tel und Kommunikationsanlagen"},
    {
        label: "0870",
        value: "Büromöbel und sonstige Geschäftsausstattung"},
    {
        label: "0880",
        value: "Reserveteile für Betriebs- und Geschäftsausstattung"},
    {
        label: "0890",
        value: "Geringwertige Vermögensgegenstände der Betriebsund Geschäftsausstattung"},
    {
        label: "0891",
        value: "GWG-Sammelposten BGA Jahr 1"},
    {
        label: "0895",
        value: "GWG-Sammelposten BGA Jahr 5"},
    {
        label: "09",
        value: "Geleistete Anzahlungen und Anlagen im Bau"},
    {
        label: "0900",
        value: "Geleistete Anzahlungen auf Sachanlagen"},
    {
        label: "0950",
        value: "Anlagen im Bau"},
    {
        label: "10",
        value: "Frei"},
    {
        label: "11",
        value: "Frei"},
    {
        label: "12",
        value: "Frei"},
    {
        label: "13",
        value: "Beteiligungen"},
    {
        label: "1300",
        value: "Beteiligungen"},
    {
        label: "14",
        value: "Frei"},
    {
        label: "15",
        value: "Wertpapiere d. Anlagevermögens"},
    {
        label: "1500",
        value: "Wertpapiere d. Anlagevermögens"},
    {
        label: "16",
        value: "Sonstige Finanzanlagen"},
    {
        label: "1600",
        value: "Sonstige Finanzanlagen"},
    {
        label: "17",
        value: "Frei"},
    {
        label: "18",
        value: "Frei"},
    {
        label: "19",
        value: "Frei"},
    {
        label: "20",
        value: "Roh-, Hilfs- und Betriebsstoffe e"},
    {
        label: "2000",
        value: "Rohstoffe/Fertigungsmaterial"},
    {
        label: "2001",
        value: "Bezugskosten"},
    {
        label: "2002",
        value: "Nachlässe"},
    {
        label: "2010",
        value: "Vorprodukte/Fremdbauteile"},
    {
        label: "2011",
        value: "Bezugskosten"},
    {
        label: "2012",
        value: "Nachlässe"},
    {
        label: "2020",
        value: "Hilfsstoffe"},
    {
        label: "2021",
        value: "Bezugskosten"},
    {
        label: "2022",
        value: "Nachlässe"},
    {
        label: "2030",
        value: "Betriebsstoffe"},
    {
        label: "2031",
        value: "Bezugskosten"},
    {
        label: "2032",
        value: "Nachlässe"},
    {
        label: "2070",
        value: "Sonstiges Material"},
    {
        label: "2071",
        value: "Bezugskosten"},
    {
        label: "2072",
        value: "Nachlässe"},
    {
        label: "21",
        value: "Unfertige Erzeugnisse, unfertige Leistungen"},
    {
        label: "2100",
        value: "Unfertige Erzeugnisse"},
    {
        label: "2190",
        value: "Unfertige Leistungen"},
    {
        label: "22",
        value: "Fertige Erzeugnisse und Waren"},
    {
        label: "2200",
        value: "Fertige Erzeugnisse"},
    {
        label: "2280",
        value: "Waren (Handelswaren) e"},
    {
        label: "2281",
        value: "Bezugskosten"},
    {
        label: "2282",
        value: "Nachlässe"},
    {
        label: "23",
        value: "Geleistete Anzahlungen a. Vorräte"},
    {
        label: "2300",
        value: "Geleistete Anzahlungen a. Vorräte"},
    {
        label: "24",
        value: "Ford. a. Lieferungen u. Leistungen"},
    {
        label: "2400",
        value: "Forderungen aus Lieferungen und Leistungen"},
    {
        label: "2420",
        value: "Kaufpreisforderungen"},
    {
        label: "2421",
        value: "Umsatzsteuerforderungen"},
    {
        label: "2450",
        value: "Wechselford. aus Lieferungen und Leistungen (Besitzwechsel)"},
    {
        label: "2470",
        value: "Zweifelhafte Forderungen"},
    {
        label: "2480",
        value: "Protestwechsel"},
    {
        label: "25",
        value: "Innergemeinschaftlicher Erwerb/Einfuhr"},
    {
        label: "2500",
        value: "Innergemeinschaftl. Erwerb"},
    {
        label: "2501",
        value: "Bezugskosten"},
    {
        label: "2502",
        value: "Nachlässe"},
    {
        label: "2510",
        value: "Gütereinfuhr"},
    {
        label: "2511",
        value: "Bezugskosten"},
    {
        label: "2512",
        value: "Nachlässe"},
    {
        label: "26",
        value: "Sonstige Vermögensgegenstände"},
    {
        label: "2600",
        value: "Vorsteuer"},
    {
        label: "2602",
        value: "Vorsteuer (19 %) für i. E."},
    {
        label: "2604",
        value: "Einfuhrumsatzsteuer"},
    {
        label: "2630",
        value: "Sonst. Ford. an Finanzbehörden"},
    {
        label: "2640",
        value: "SV-Vorauszahlung"},
    {
        label: "2650",
        value: "Forderungen an Mitarbeiter"},
    {
        label: "2690",
        value: "Übrige sonstige Forderungen"},
    {
        label: "27",
        value: "Wertpapiere des Umlaufvermögens"},
    {
        label: "2700",
        value: "Wertpapiere des Umlaufvermögens"},
    {
        label: "28",
        value: "Flüssige Mittel"},
    {
        label: "2800",
        value: "Guthaben bei Kreditinstituten (Bank)"},
    {
        label: "2850",
        value: "Postbank"},
    {
        label: "2860",
        value: "Schecks"},
    {
        label: "2870",
        value: "Bundesbank"},
    {
        label: "2880",
        value: "Kasse"},
    {
        label: "2890",
        value: "Nebenkassen"},
    {
        label: "29",
        value: "Aktive Rechnungsabgrenzung (und Bilanzfehlbetrag)"},
    {
        label: "2900",
        value: "Aktive Jahresabgrenzung"},
    {
        label: "2920",
        value: "Umsatzsteuer auf erhaltene Anzahlungen"},
    {
        label: "2930",
        value: "Disagio"},
    {
        label: "2990",
        value: "(nicht durch Eigenkapital gedeckter Fehlbetrag)"},
    {
        label: "30",
        value: "Eigenkapital/ Gezeichnetes Kapital"},
    {
        label: "3000",
        value: "Eigenkapital"},
    {
        label: "3001",
        value: "Privatkonto Bei Personengesellschaften"},
    {
        label: "3000",
        value: "Kapital Gesellschafter A"},
    {
        label: "3001",
        value: "Privatkonto A"},
    {
        label: "3010",
        value: "Kapital Gesellschafter B"},
    {
        label: "3011",
        value: "Privatkonto B"},
    {
        label: "3070",
        value: "Kommanditkapital Gesellschafter C"},
    {
        label: "3080",
        value: "Kommanditkapital Gesellschafter D"},
    {
        label: "3000",
        value: "Gezeichnetes Kapital (Grundkapital/Stammkapital)"},
    {
        label: "31",
        value: "Kapitalrücklage"},
    {
        label: "3100",
        value: "Kapitalrücklage"},
    {
        label: "32",
        value: "Gewinnrücklagen"},
    {
        label: "3210",
        value: "Gesetzliche Rücklagen"},
    {
        label: "3230",
        value: "Satzungsmäßige Rücklagen"},
    {
        label: "3240",
        value: "Andere Gewinnrücklagen"},
    {
        label: "33",
        value: "Ergebnisverwendung b"},
    {
        label: "3310",
        value: "Jahresergebnis des Vorjahres"},
    {
        label: "3320",
        value: "Ergebnisvortrag aus früheren Perioden"},
    {
        label: "3340",
        value: "Veränderung der Rücklagen"},
    {
        label: "3350",
        value: "Bilanzgewinn/Bilanzverlust"},
    {
        label: "3360",
        value: "Ergebnisausschüttung"},
    {
        label: "3390",
        value: "Ergebnisvortrag auf neue Rechnung"},
    {
        label: "34",
        value: "Jahresüberschuss/ Jahresfehlbetrag"},
    {
        label: "3400",
        value: "Jahresüberschuss/ Jahresfehlbetrag"},
    {
        label: "35",
        value: "Sonderposten mit Rücklageanteil"},
    {
        label: "3500",
        value: "Sonderposten mit Rücklageanteil"},
    {
        label: "36",
        value: "Wertberichtigungen (Bei Kapitalgesellschaften als Passivposten der Bilanz nicht mehr zulässig)"},
    {
        label: "3610",
        value: "zu Sachanlagen"},
    {
        label: "3650",
        value: "zu Finanzanlagen"},
    {
        label: "3670",
        value: "Einzelwertberichtigung zu Forderungen"},
    {
        label: "3680",
        value: "Pauschalwertberichtigung zu Forderungen Rückstellungen"},
    {
        label: "37",
        value: "Rückstellungen für Pensionen und ähnliche Verpflichtungen"},
    {
        label: "3700",
        value: "Rückstellungen für Pensionen und ähnliche Verpflichtungen"},
    {
        label: "38",
        value: "Steuerrückstellungen"},
    {
        label: "3800",
        value: "Steuerrückstellungen"},
    {
        label: "39",
        value: "Sonstige Rückstellungen c"},
    {
        label: "3910",
        value: "für Gewährleistung"},
    {
        label: "3930",
        value: "für andere ungewisse Verbindlichkeiten"},
    {
        label: "3970",
        value: "für drohende Verluste aus schwebenden Geschäften"},
    {
        label: "3990",
        value: "für Aufwendungen"},
    {
        label: "40",
        value: "Frei"},
    {
        label: "41",
        value: "Anleihen"},
    {
        label: "4100",
        value: "Anleihen"},
    {
        label: "42",
        value: "Verbindlichkeiten gegenüber Kreditinstituten"},
    {
        label: "4210",
        value: "Kurzfristige Bankverbindlichkeiten"},
    {
        label: "4230",
        value: "Mittelfristige Bankverbindlichkeiten"},
    {
        label: "4250",
        value: "Langfristige Bankverbindlichkeiten"},
    {
        label: "43",
        value: "Erhaltene Anzahlungen auf Bestellungen"},
    {
        label: "4300",
        value: "Erhaltene Anzahlungen"},
    {
        label: "44",
        value: "Verbindlichkeiten aus Lieferungen und Leistungen"},
    {
        label: "4400",
        value: "Verbindlichkeiten aus Lieferungen und Leistungen"},
    {
        label: "4420",
        value: "Kaufpreisverbindlichkeiten"},
    {
        label: "45",
        value: "Wechselverbindlichkeiten"},
    {
        label: "4500",
        value: "Schuldwechsel"},
    {
        label: "46",
        value: "Frei"},
    {
        label: "47",
        value: "Frei"},
    {
        label: "48",
        value: "Sonstige Verbindlichkeiten"},
    {
        label: "4800",
        value: "Umsatzsteuer"},
    {
        label: "4802",
        value: "Umsatzsteuer (19 %) für i. E."},
    {
        label: "4820",
        value: "Zollverbindlichkeiten"},
    {
        label: "4830",
        value: "Sonstige Verbindlichkeiten gegenüber Finanzbehörden"},
    {
        label: "4850",
        value: "Verbindlichkeiten gegenüber Mitarbeitern"},
    {
        label: "4860",
        value: "Verbindlichkeiten aus vermö- genswirksamen Leistungen"},
    {
        label: "4870",
        value: "Verbindlichkeiten gegenüber Gesellschaftern (Dividende)"},
    {
        label: "4890",
        value: "Übrige sonstige Verbindlichkeiten"},
    {
        label: "49",
        value: "Passive Rechnungsabgrenzung"},
    {
        label: "4900",
        value: "Passive Jahresabgrenzung"},
    {
        label: "50",
        value: "Umsatzerlöse für eigene Erzeugnisse u. andere eigene Leistungen"},
    {
        label: "5000",
        value: "Umsatzerlöse f. eigene Erzeugn."},
    {
        label: "5001",
        value: "Erlösberichtigungen"},
    {
        label: "5050",
        value: "Umsatzerlöse für andere eigene Leistungen"},
    {
        label: "5051",
        value: "Erlösberichtigungen"},
    {
        label: "5060",
        value: "Erlöse aus innergemeinschaftlicher Lieferung (i. L.)"},
    {
        label: "5061",
        value: "Erlösberichtigungen"},
    {
        label: "5070",
        value: "Erlöse aus Güterausfuhr"},
    {
        label: "5071",
        value: "Erlösberichtigungen"},
    {
        label: "51",
        value: "Umsatzerlöse für Waren und sonstige Umsatzerlöse"},
    {
        label: "5100",
        value: "Umsatzerlöse für Waren"},
    {
        label: "5101",
        value: "Erlösberichtigungen"},
    {
        label: "5190",
        value: "Sonstige Umsatzerlöse"},
    {
        label: "5191",
        value: "Erlösberichtigungen"},
    {
        label: "52",
        value: "Erhöhung oder Verminderung des Bestandes an unfertigen und fertigen Erzeugnissen"},
    {
        label: "5200",
        value: "Bestandsveränderungen"},
    {
        label: "5201",
        value: "Bestandsveränderungen an unfertigen Erzeugnissen und nicht abgerechneten Leistungen"},
    {
        label: "5202",
        value: "Bestandsveränderungen an fertigen Erzeugnissen"},
    {
        label: "53",
        value: "Andere aktivierte Eigenleistungen"},
    {
        label: "5300",
        value: "Aktivierte Eigenleistungen"},
    {
        label: "54",
        value: "Sonstige betriebliche Erträge"},
    {
        label: "5400",
        value: "Mieterträge"},
    {
        label: "5401",
        value: "Leasingerträge"},
    {
        label: "5410",
        value: "Sonstige Erlöse (z. B. aus Provisionen oder Anlagenabgängen)"},
    {
        label: "5420",
        value: "Entnahme von Gegenständen und sonstigen Leistungen"},
    {
        label: "5430",
        value: "Andere sonstige betriebl. Erträge"},
    {
        label: "5440",
        value: "Erträge aus Werterhöhungen von Gegenständen des Anlage-vermögens(Zuschreibungen)"},
    {
        label: "5441",
        value: "Erträge aus Zuschreibungen zum Umlaufvermögen"},
    {
        label: "5450",
        value: "Erträge aus der Auflösung oder Herabsetzung von Wertberichtigungen auf Forderungen"},
    {
        label: "5480",
        value: "Erträge aus der Herabsetzung von Rückstellungen"},
    {
        label: "5490",
        value: "Periodenfremde Erträge"},
    {
        label: "55",
        value: "Erträge aus Beteiligungen"},
    {
        label: "5500",
        value: "Erträge aus Beteiligungen"},
    {
        label: "56",
        value: "Erträge aus anderen Wertpapieren und Ausleihungen des Finanzanlagevermögens"},
    {
        label: "5600",
        value: "Erträge aus anderen Finanzanlagen"},
    {
        label: "57",
        value: "Sonstige Zinsen und ähnliche Erträge"},
    {
        label: "5710",
        value: "Zinserträge"},
    {
        label: "5730",
        value: "Diskonterträge"},
    {
        label: "5780",
        value: "Erträge aus Wertpapieren des Umlaufvermögens"},
    {
        label: "5790",
        value: "Sonstige zinsähnliche Erträge"},
    {
        label: "58",
        value: "Außerordentliche Erträge"},
    {
        label: "5800",
        value: "Außerordentliche Erträge"},
    {
        label: "59",
        value: "Frei"},
    {
        label: "60",
        value: "Aufwendungen für Roh-, Hilfs-und Betriebsstoffe und für bezogene Waren e"},
    {
        label: "6000",
        value: "Aufwendungen für Rohstoffe/Fertigungsmaterial"},
    {
        label: "6001",
        value: "Bezugskosten"},
    {
        label: "6002",
        value: "Nachlässe"},
    {
        label: "6010",
        value: "Aufwendungen für Vorprodukte/Fremdbauteile e"},
    {
        label: "6020",
        value: "Aufwendungen für Hilfsstoffe e"},
    {
        label: "6030",
        value: "Aufwendungen für Betriebsstoffe/Verbrauchswerkzeuge e"},
    {
        label: "6040",
        value: "Aufw. für Verpackungsmaterial"},
    {
        label: "6050",
        value: "Aufw. für Energie u. Treibstoffe"},
    {
        label: "6060",
        value: "Aufw. für Reparaturmaterial"},
    {
        label: "6070",
        value: "Aufwendungen für sonstiges Material"},
    {
        label: "6080",
        value: "Aufwendungen für Waren e"},
    {
        label: "61",
        value: "Aufwendungen für bezogene Leistungen"},
    {
        label: "6100",
        value: "Fremdleistungen für Erzeugnisse und andere Umsatzleistungen"},
    {
        label: "6140",
        value: "Frachten und Fremdlager"},
    {
        label: "6150",
        value: "Vertriebsprovisionen"},
    {
        label: "6160",
        value: "Fremdinstandhaltung"},
    {
        label: "6170",
        value: "Sonstige Aufwendungen für bezogene Leistungen"},
    {
        label: "62",
        value: "Löhne"},
    {
        label: "6200",
        value: "Löhne einschl. tariflicher, vertraglicher oder arbeitsbedingter Zulagen"},
    {
        label: "6210",
        value: "Urlaubs- und Weihnachtsgeld"},
    {
        label: "6220",
        value: "Sonstige tarifliche oder vertragliche Aufwendungen für Lohnempfänger"},
    {
        label: "6230",
        value: "Freiwillige Zuwendungen"},
    {
        label: "6250",
        value: "Sachbezüge"},
    {
        label: "6260",
        value: "Vergütungen an gewerbliche Auszubildende"},
    {
        label: "63",
        value: "Gehälter"},
    {
        label: "6300",
        value: "Gehälter und Zulagen"},
    {
        label: "6310",
        value: "Urlaubs- und Weihnachtsgeld"},
    {
        label: "6320",
        value: "Sonstige tarifliche oder vertragliche Aufwendungen"},
    {
        label: "6330",
        value: "Freiwillige Zuwendungen"},
    {
        label: "6350",
        value: "Sachbezüge"},
    {
        label: "6360",
        value: "Vergütungen an Auszubildende"},
    {
        label: "64",
        value: "Soziale Abgaben und Aufwendungen für Altersversorgung und für Unterstützung"},
    {
        label: "6400",
        value: "Arbeitgeberanteil zur Sozialversicherung (Lohnbereich) d"},
    {
        label: "6410",
        value: "Arbeitgeberanteil zur Sozialversicherung (Gehaltsbereich) d"},
    {
        label: "6420",
        value: "Beiträge zur Berufsgenossenschaft"},
    {
        label: "6440",
        value: "Aufwendungen für Altersversorgung"},
    {
        label: "6490",
        value: "Aufwendungen für Unterstützung"},
    {
        label: "6495",
        value: "Sonstige soziale Aufwendungen"},
    {
        label: "65",
        value: "Abschreibungen auf Anlagevermögen"},
    {
        label: "6510",
        value: "Abschreibungen auf immaterielle Vermögensgegenstände des Anlagevermögens"},
    {
        label: "6520",
        value: "Abschreibungen auf Sachanlagen"},
    {
        label: "6540",
        value: "Abschreibungen auf geringwertige Wirtschaftsgüter"},
    {
        label: "6541",
        value: "Abschreibungen auf GWG-Sammelposten Jahr 1"},
    {
        label: "6545",
        value: "Abschreibungen auf GWG-Sammelposten Jahr 5"},
    {
        label: "6550",
        value: "Außerplanmäßige Abschreibungen auf Sachanlagen"},
    {
        label: "6570",
        value: "Unüblich hohe Abschreibungen auf Umlaufvermögen"},
    {
        label: "66",
        value: "Sonstige Personalaufwendungen"},
    {
        label: "6600",
        value: "Aufwendungen für Personaleinstellung"},
    {
        label: "6610",
        value: "Aufwendungen für übernommene Fahrtkosten"},
    {
        label: "6620",
        value: "Aufwendungen für Werksarzt und Arbeitssicherheit"},
    {
        label: "6630",
        value: "Personenbezogene Versicherungen"},
    {
        label: "6640",
        value: "Aufwendungen für Fort- und Weiterbildung"},
    {
        label: "6650",
        value: "Aufwendungen für Dienstjubiläen"},
    {
        label: "6660",
        value: "Aufwendungen für Belegschaftsveranstaltungen"},
    {
        label: "6670",
        value: "Aufwendungen für Werksküche und Sozialeinrichtungen"},
    {
        label: "6680",
        value: "Ausgleichsabgabe nach dem Schwerbehindertengesetz"},
    {
        label: "6690",
        value: "Übrige sonstige Personalaufwendungen"},
    {
        label: "67",
        value: "Aufwendungen für die Inanspruchnahme von Rechten und Diensten"},
    {
        label: "6700",
        value: "Mieten, Pachten"},
    {
        label: "6710",
        value: "Leasingaufwendungen"},
    {
        label: "6720",
        value: "Lizenzen und Konzessionen"},
    {
        label: "6730",
        value: "Gebühren"},
    {
        label: "6750",
        value: "Kosten des Geldverkehrs"},
    {
        label: "6760",
        value: "Provisionsaufwendungen (außer Vertriebsprovisionen)"},
    {
        label: "6770",
        value: "Rechts- und Beratungskosten"},
    {
        label: "68",
        value: "Aufwendungen für Kommunikation(Dokumentation, Information, Reisen, Werbung)"},
    {
        label: "6800",
        value: "Büromaterial"},
    {
        label: "6810",
        value: "Zeitungen und Fachliteratur"},
    {
        label: "6820",
        value: "Portokosten"},
    {
        label: "6830",
        value: "Kosten der Telekommunikation"},
    {
        label: "6850",
        value: "Reisekosten"},
    {
        label: "6860",
        value: "Bewirtung und Präsentation"},
    {
        label: "6870",
        value: "Werbung"},
    {
        label: "6880",
        value: "Spenden"},
    {
        label: "69",
        value: "Aufwendungen für Beiträge und Sonstiges sowie Wertkorrekturen und periodenfremde Aufwendungen"},
    {
        label: "6900",
        value: "Versicherungsbeiträge"},
    {
        label: "6920",
        value: "Beiträge zu Wirtschaftsverbänden und Berufsvertretungen"},
    {
        label: "6930",
        value: "Verluste aus Schadensfällen"},
    {
        label: "6940",
        value: "Sonstige Aufwendungen"},
    {
        label: "6950",
        value: "Abschreibungen auf Forderungen"},
    {
        label: "6951",
        value: "Abschreibungen auf Forderungen wegen Uneinbringlichkeit"},
    {
        label: "6952",
        value: "Einstellung in Einzelwertberichtigung"},
    {
        label: "6953",
        value: "Einstellung in Pauschalwertberichtigung"},
    {
        label: "6960",
        value: "Verluste aus dem Abgang von Vermögensgegenständen"},
    {
        label: "6979",
        value: "Anlagenabgänge"},
    {
        label: "6980",
        value: "Zuführungen zu Rückstellungen für Gewährleistung"},
    {
        label: "6990",
        value: "Periodenfremde Aufwendungen"},
    {
        label: "70",
        value: "Betriebliche Steuern"},
    {
        label: "7020",
        value: "Grundsteuer"},
    {
        label: "7021",
        value: "Grundsteuer – Vorjahr"},
    {
        label: "7030",
        value: "Kraftfahrzeugsteuer"},
    {
        label: "7031",
        value: "Kraftfahrzeugsteuer – Vorjahr"},
    {
        label: "7032",
        value: "Steuerrückerstattung für KfzSteuer – Vorjahr"},
    {
        label: "7070",
        value: "Ausfuhrzölle"},
    {
        label: "7080",
        value: "Verbrauchsteuern"},
    {
        label: "7090",
        value: "Sonstige betriebliche Steuern"},
    {
        label: "71",
        value: "Frei"},
    {
        label: "72",
        value: "Frei"},
    {
        label: "73",
        value: "Frei"},
    {
        label: "74",
        value: "Abschreibungen auf Finanzanlagen und auf Wertpapiere des Umlaufvermögens und Verluste aus entsprechenden Abgängen"},
    {
        label: "7400",
        value: "Abschreibungen auf Finanzanlagen"},
    {
        label: "7420",
        value: "Abschreibungen auf Wertpapiere des Umlaufvermögens"},
    {
        label: "7450",
        value: "Verluste aus dem Abgang von Finanzanlagen"},
    {
        label: "7460",
        value: "Verluste aus dem Abgang von Wertpapieren des Umlaufvermögens"},
    {
        label: "75",
        value: "Zinsen und ähnliche Aufwendungen"},
    {
        label: "7510",
        value: "Zinsaufwendungen"},
    {
        label: "7530",
        value: "Diskontaufwendungen"},
    {
        label: "7590",
        value: "Sonstige zinsähnliche Aufwendungen"},
    {
        label: "76",
        value: "Außerordentliche Aufwendungen"},
    {
        label: "7600",
        value: "Außerordentliche Aufwendungen"},
    {
        label: "77",
        value: "Steuern vom Einkommen und Ertrag"},
    {
        label: "7700",
        value: "Gewerbesteuer"},
    {
        label: "7701",
        value: "Gewerbesteuer – Vorjahr"},
    {
        label: "7702",
        value: "Steuerrückerstattung für Gewerbesteuer – Vorjahr"},
    {
        label: "7710",
        value: "Körperschaftsteuer"},
    {
        label: "7720",
        value: "Kapitalertragsteuer"},
    {
        label: "78",
        value: "Diverse Aufwendungen"},
    {
        label: "7800",
        value: "Diverse Aufwendungen"},
    {
        label: "79",
        value: "Frei"},
    {
        label: "80",
        value: "Eröffnung/Abschluss"},
    {
        label: "8000",
        value: "Eröffnungsbilanzkonto"},
    {
        label: "8010",
        value: "Schlussbilanzkonto"},
    {
        label: "8020",
        value: "GuV-Konto Gesamtkostenverfahren"},
    {
        label: "8030",
        value: "GuV-Konto Umsatzkostenverfahren"},
    {
        label: "8050",
        value: "Saldenvorträge(Sammelkonto)Konten der Kostenbereiche für die GuV im Umsatzkostenverfahren"},
    {
        label: "81",
        value: "Herstellungskosten"},
    {
        label: "82",
        value: "Vertriebskosten"},
    {
        label: "83",
        value: "Allgemeine Verwaltungskosten"},
    {
        label: "84",
        value: "Sonstige betriebliche Aufwendungen Konten der kurzfristigen Erfolgsrechnung (KER) für innerjährige Rechnungsperioden (Monat, Quartal oder Halbjahr)"},
    {
        label: "85",
        value: "Korrekturkonten zu den Erträgen der Kontenklasse 5"},
    {
        label: "86",
        value: "Korrekturkonten zu den Aufwendungen der Kontenklasse 6"},
    {
        label: "87",
        value: "Korrekturkonten zu den Aufwendungen der Kontenklasse 7"},
    {
        label: "88",
        value: "Kurzfristige Erfolgsrechnung (KER)"},
    {
        label: "8800",
        value: "Gesamtkostenverfahren"},
    {
        label: "8810",
        value: "Umsatzkostenverfahren"},
    {
        label: "89",
        value: "Innerjährige Rechnungsabgrenzung"},
    {
        label: "8900",
        value: "Aktive Rechnungsabgrenzung"},
    {
        label: "90",
        value: "Unternehmensbezogene Abgrenzungen (neutrale Aufwendungen u. Erträge)"},
    {
        label: "91",
        value: "Kostenrechnerische Korrekturen"},
    {
        label: "92",
        value: "Kostenarten und Leistungsarten"},
    {
        label: "93",
        value: "Kostenstellen"},
    {
        label: "94",
        value: "Kostenträger"},
    {
        label: "95",
        value: "Fertige Erzeugnisse"},
    {
        label: "96",
        value: "Interne Lieferungen und Leistungen sowie deren Kosten"},
    {
        label: "97",
        value: "Umsatzkosten"},
    {
        label: "98",
        value: "Umsatzleistungen"},
    {
        label: "99",
        value: "Ergebnisausweise"}
    ];

  $("#journal_from_account").autocomplete({
    source: value_konto_nummer_kontenplan,
    minLength: 3,
    select: function(event, ui) {
        event.preventDefault();
        $("#journal_from_account").val(ui.item.label);
        $("#journal_from_account_number").val(ui.item.value);
    },
    focus: function(event, ui) {
        event.preventDefault();
    $("#journal_from_account").val(ui.item.label);
    }
  });
  
  $("#journal_from_account_number").autocomplete({
    source: value_konto_name_kontenplan,
    minLength: 2,
    select: function(event, ui) {
        event.preventDefault();
        $("#journal_from_account").val(ui.item.value);
        $("#journal_from_account_number").val(ui.item.label);
    },
    focus: function(event, ui) {
        event.preventDefault();
        $("#journal_from_account").val(ui.item.value);
    }
  });
  
  $("#journal_to_account").autocomplete({
    source: value_konto_nummer_kontenplan,
    minLength: 3,
    select: function(event, ui) {
        event.preventDefault();
        $("#journal_to_account").val(ui.item.label);
        $("#journal_to_account_number").val(ui.item.value);
    },
    focus: function(event, ui) {
        event.preventDefault();
    $("#journal_to_account").val(ui.item.label);
    }
  });
  
  $("#journal_to_account_number").autocomplete({
    source: value_konto_name_kontenplan,
    minLength: 2,
    select: function(event, ui) {
        event.preventDefault();
        $("#journal_to_account").val(ui.item.value);
        $("#journal_to_account_number").val(ui.item.label);
    },
    focus: function(event, ui) {
        event.preventDefault();
        $("#journal_to_account").val(ui.item.value);
    }
  });
});