# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
@linkIt = (element, link) ->
  window.location.href = link
 
$(document).on 'turbolinks:load', ->
  $("td[data-link]").click (e) ->
    e.preventDefault()

    linkIt(this, $(this).data("link"))