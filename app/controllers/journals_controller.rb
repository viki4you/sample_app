class JournalsController < ApplicationController
  before_action :set_journal, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user
  before_action :correct_user

  # GET /journals
  # GET /journals.json
  def index
    @user = User.find(params[:user_id])
    @journals = getJournal(params[:user_id])
    #@journals = @user.journals.all
  end

  # GET /journals/1
  # GET /journals/1.json
  def show
    #@user = User.find(params[:user_id])
    @hauptbuch = createHauptbuch(params[:user_id])
    #@tmp = "#{Rails.root}/google"
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "show", :template => "journals/show.html.erb"
      end
    end
  end

  # GET /journals/new
  def new
    @user = User.find(params[:user_id])
    @journal = @user.journals.new
    @kontenplan = IkrKontenplan.all
  end

  # GET /journals/1/edit
  def edit
    @user = User.find(params[:user_id])
  end

  # POST /journals
  # POST /journals.json
  def create
    @user = User.find(params[:user_id])
    #@journal = Journal.new(journal_params)
    @journal = @user.journals.create(journal_params)
    
    if params[:umsatzsteuer][:soll_umsatzsteuer_id].empty? && 
      params[:umsatzsteuer][:haben_umsatzsteuer_id].empty?
      if @journal.save
        flash[:success] = "Journaleintrag erstellt"
        redirect_to new_user_journal_url(@user)
  #byebug
      else
        render 'new'
      end
    elsif params[:umsatzsteuer][:soll_umsatzsteuer_id] == "19"
      umsatzsteuerBuchen(@journal, "19", "soll")
    elsif params[:umsatzsteuer][:soll_umsatzsteuer_id] == "7"
      @journal.from_amount = (@journal.from_amount.to_f / 1.07).to_s
      @journal.to_amount = @journal.from_amount
      
      @umsatzsteuer = @journal.dup
      
      @umsatzsteuer.from_amount = (@umsatzsteuer.from_amount.to_f * 0.07).to_s
      @umsatzsteuer.to_amount = @umsatzsteuer.from_amount
      @umsatzsteuer.to_account = "Umsatzsteuer"
      @umsatzsteuer.to_account_number = "4800"
      if (@umsatzsteuer.save && @journal.save)
        flash[:success] = "2 Journaleinträge erstellt"
        redirect_to new_user_journal_url(@user)
      else  
        render 'new'
      end
    elsif params[:umsatzsteuer][:haben_umsatzsteuer_id] == "19"
      
    end

    #respond_to do |format|
      #if @journal.save
        #format.html { redirect_to @journal, notice: 'Journal was successfully created.' }
        #format.json { render :show, status: :created, location: @journal }
      #else
        #format.html { render :new }
        #format.json { render json: @journal.errors, status: :unprocessable_entity }
      #end
    #end
  end

  # PATCH/PUT /journals/1
  # PATCH/PUT /journals/1.json
  def update
    @user = User.find(params[:user_id])
    #@journal = Journal.new(journal_params)
    
    if @journal.update(journal_params)
      redirect_to user_journal_url(@user, @journal)
    end
    
    #respond_to do |format|
      #if @journal.update(journal_params)
        #format.html { redirect_to(@journal, notice: 'Journal was successfully updated.') }
        #format.json { render :show, status: :ok, location: @journal }
      #else
        #format.html { render :edit }
        #format.json { render json: @journal.errors, status: :unprocessable_entity }
      #end
    #end
  end

  # DELETE /journals/1
  # DELETE /journals/1.json
  def destroy
    @journal.destroy
    respond_to do |format|
      format.html { redirect_to user_journals_url, notice: 'Journal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def hauptbuch
    @hauptbuch = createHauptbuch(params[:user_id])
  end
  
  def konto
    createKonto(params[:user_id], params[:query])
  end
  
  def createPdf
    @hauptbuch = createHauptbuch(params[:user_id])
    @hauptbuch.each do |row|
# Exclude "Eigenkapital" from abschluss. "Eigenkapital" abschluss is done in
# createGuVBuchung
      if row[0].to_i != 3000
        @konto = createKonto(params[:user_id], row[0])
        kontoAbschliessen(@konto, row[0], row[1])
      end
    end
    
    createGuVBuchung(params[:user_id])
    
    @hauptbuch = createHauptbuch(params[:user_id])
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "abschluss", :template => "journals/abschluss.html.erb"
               #, :page_height =>  1
      end
    end
  end
  
  def abschluss
    @hauptbuch = createHauptbuch(params[:user_id])
    @hauptbuch.each do |row|
# Exclude "Eigenkapital" from abschluss. "Eigenkapital" abschluss is done in
# createGuVBuchung
      if row[0].to_i != 3000
        @konto = createKonto(params[:user_id], row[0])
        kontoAbschliessen(@konto, row[0], row[1])
      end
    end
    
    createGuVBuchung(params[:user_id])
    
    @hauptbuch = createHauptbuch(params[:user_id])
=begin
    #@entry_created = true
    @hauptbuch = createHauptbuch(params[:user_id])
    @hauptbuch.each do |row|
# abschluss buchen ohne Eroeffnungsbilanzkonto
      if ((row[0].to_i != 8020) and (row[0].to_i != 8000) and (row[0].to_i != 3000))
        @konto = createKonto(params[:user_id], row[0])
        kontoAbschliessen(@konto, row[0], row[1])
      end
    end
    createGuVBuchung(params[:user_id])
    @konto = createKonto(params[:user_id], "8010")
    eroeffnung(@konto, params[:user_id])
=end
  end
  
  helper_method :eroeffnung, :createKonto
  
  private
  
    def umsatzsteuerBuchen(journal, prozentsatz, soll_oder_haben)
      journal.from_amount = (journal.from_amount.to_f / 1.19).to_s
=begin      
      @journal.to_amount = @journal.from_amount
      
      @umsatzsteuer = @journal.dup
      
      @umsatzsteuer.from_amount = (@umsatzsteuer.from_amount.to_f * 0.19).to_s
      @umsatzsteuer.to_amount = @umsatzsteuer.from_amount
      @umsatzsteuer.to_account = "Umsatzsteuer"
      @umsatzsteuer.to_account_number = "4800"
      if (@umsatzsteuer.save && @journal.save)
        flash[:success] = "2 Journaleinträge erstellt"
        redirect_to new_user_journal_url(@user)
      else  
        render 'new'
      end
=end
    end
    
# create buchung from GuV nach Eigenkapital
    def createGuVBuchung(user_id)
      @konto = createKonto(user_id, "8020")
      kontoAbschliessen(@konto, "8020", "GuV-Konto Gesamtkostenverfahren")
      #kontoAbschliessen(@konto, "8020", "GuV-Konto Gesamtkostenverfahren")
      @konto = createKonto(user_id, "3000")
      kontoAbschliessen(@konto, "3000", "Eigenkapital")
    end
  
    def createHauptbuch(user_id)
      @user = User.find(user_id)
      @journals = getJournal(user_id)
      #@soll_journals = @journals.distinct.pluck(:from_account_number, :from_account)
      #@haben_journals = @journals.distinct.pluck(:to_account_number, :to_account)
      @soll_journals = @journals.select('from_account_number, from_account')
      @haben_journals = @journals.select('to_account_number, to_account')
      return_array = Array.new
      @journals.each { |row|
        return_array << Array.new([row.from_account_number, row.from_account])
        return_array << Array.new([row.to_account_number, row.to_account])
      }
#      @retur_array.uniq { |s| s.first }
      return_array = return_array.uniq{|x| x[0]}
      return_array = return_array.sort_by{|x| x[0]}
#      return_array = return_array.sort_by{ |a| a[0] }
#byebug
#      @soll_journals.concat @haben_journals
#      @soll_journals = @soll_journals.uniq

      #@soll_journals = @soll_journals.order(:from_account_number)
#      @soll_journals = @soll_journals.dup.sort_by{ |a| a[0] }
      #@soll_journals = @soll_journals.sort_by{ |a| a[0] }

      return return_array
    end
  
    def createKonto(user_id, konto_number)
      @user = User.find(user_id)
      @konto_number = konto_number
      
      soll_journals = Array.new
      haben_journals = Array.new
      
      @journals = getJournal(user_id)
      @journals.each do |row|
        if row.from_account_number == @konto_number
          soll_journals << Array.new([row.to_account_number, row.to_account, row.to_amount])
        end
        if row.to_account_number == @konto_number
          haben_journals << Array.new([row.from_account_number, row.from_account, row.from_amount])
        end
      end
=begin
      @soll_journals = @journals.where(from_account_number: @konto_number).pluck(
                                        :to_account_number, :to_account,
                                        :to_amount)
      @haben_journals = @journals.where(to_account_number: @konto_number).pluck(
                                        :from_account_number, :from_account,
                                        :from_amount)
=end
      @rows_count = soll_journals.count
      if @rows_count < haben_journals.count
        @rows_count = haben_journals.count
      end
      
      @konto = Array.new
      for i in 0..(@rows_count - 1)
        if soll_journals[i].nil? then
          soll_journals[i] = Array.new(["-", "-", "-"])
        end
        if haben_journals[i].nil? then
          haben_journals[i] = Array.new(["-", "-", "-"])
        end
  
        konto_row = Array.new([soll_journals[i][0],
                                    soll_journals[i][1],
                                    soll_journals[i][2],
                                    haben_journals[i][0],
                                    haben_journals[i][1],
                                    haben_journals[i][2]])
        @konto << konto_row
      end
      return @konto
    end
    
    def kontoAbschliessen(konto, konto_number, konto_name, guv = false)
      soll_betrag = 0
      haben_betrag = 0
      konto.each do |konto_line|
        if konto_line[2] != '-'
          soll_betrag += konto_line[2].to_i
        end
        if konto_line[5] != '-'
          haben_betrag += konto_line[5].to_i
        end
      end
      
      betrag = soll_betrag - haben_betrag
      
#      targetKontoNummer = "8020"
#      targetKontoName = "GuV-Konto Gesamtkostenverfahren"

# Wenn Bestandskonto
      if konto_number[0, 1].to_i < 5
        targetKontoNummer = "8010"
        targetKontoName = "Schlussbilanzkonto"
# Wenn Erfolgskonto
      elsif konto_number[0, 1].to_i < 8
        targetKontoNummer = "8020"
        targetKontoName = "GuV-Konto Gesamtkostenverfahren"
# Wenn GuV
      elsif konto_number.to_i == 8020
        targetKontoNummer = "3000"
        targetKontoName = "Eigenkapital"
      end
      
      if betrag < 0 
        betrag *= -1
        @journal = @user.journals.create(:from_account_number => konto_number,
                        :from_account => konto_name,
                        :from_amount => betrag.to_s,
                        :to_account_number => targetKontoNummer,
                        :to_account => targetKontoName,
                        :to_amount => betrag.to_s)
# when Passivkontobetrag == 0 standartbuchung
      else
        #betrag += 1000
        @journal = @user.journals.create(:from_account_number => targetKontoNummer,
                        :from_account => targetKontoName,
                        :from_amount => betrag.to_s,
                        :to_account_number => konto_number,
                        :to_account => konto_name,
                        :to_amount => betrag.to_s)
      end

      if @journal.save
          #@erstellte_eintraege += 1
          #flash[:success] = "Journaleintrag erstellt"
          #redirect_to new_user_journal_url(@user)
        else
          
      end
    end
    
    def eroeffnung(user_id)
      @user = User.find(user_id)
      
      konto = createKonto(user_id, "8010")
      
# add timestamp to journal for viewing different abschlusses
      #@timestamp = DateTime.now.strftime('%Y-%m-%d %H:%M:%S.%N').to_s
=begin
      @user.journals.where(backup: false).each do |journal|
        journal.backup = true
        #journal.abschlussname = nil
        journal.save
      end
=end
      @journals = getJournal(user_id)
      @journals.each do |journal|
        if journal.backup == false
          journal.backup = true
        end
        #journal.abschlussname = nil
        journal.save
      end
      
      konto.each do |konto_line|
        if konto_line[0] != "-"
          @journal = @user.journals.create(:from_account_number => konto_line[0],
                        :from_account => konto_line[1],
                        :from_amount => konto_line[2].to_s,
                        :to_account_number => "8000",
                        :to_account => "Eröffnungsbilanzkonto",
                        :to_amount => konto_line[2].to_s)
          @journal.save
        end
        if konto_line[3] != "-"
          @journal = @user.journals.create(:from_account_number => "8000",
                        :from_account => "Eröffnungsbilanzkonto",
                        :from_amount => konto_line[5].to_s,
                        :to_account_number => konto_line[3],
                        :to_account => konto_line[4],
                        :to_amount => konto_line[5].to_s)
          @journal.save
        end
      end
    end
    
    def getJournal(user_id)
      @user = User.find(user_id)
      #@journal = @user.journals.all
      @journal = @user.journals.where(backup: false).order(id: :desc)
    end
  
    # Use callbacks to share common setup or constraints between actions.
    def set_journal
      @journal = Journal.find(params[:id])
    end
    
    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:user_id])
      redirect_to(root_url) unless current_user?(@user)
      log_out unless current_user?(@user)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def journal_params
      #params.require(:journal).permit(:user_id)
      params.require(:journal).permit(:user_id,
                                      :from_account_number,
                                      :from_account,
                                      :from_amount,
                                      :to_account_number,
                                      :to_account,
                                      :to_amount,
                                      :backup)
    end
    
    def umsatzsteuer_params
      params.require(:Umsatzsteuer).permit(:soll_umsatzsteuer_id)
    end
end
