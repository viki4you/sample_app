#encoding: utf-8

class KontoValidator < ActiveModel::Validator
  def validate(journal)
    error_text = " existiert nicht"
    if !(IkrKontenplan.exists?(:nummer => journal.from_account_number))
      journal.errors.add(:from_account_number, :Soll_Konto_Nummer,
        message: error_text)
    end
    if !(IkrKontenplan.exists?(:nummer => journal.to_account_number))
      journal.errors.add(:to_account_number, :Haben_Konto_Nummer,
        message: error_text)
    end
    if !(IkrKontenplan.exists?(:name => journal.from_account))
      journal.errors.add(:from_account, :Soll_Konto,
        message: error_text)
    end
    if !(IkrKontenplan.exists?(:name => journal.to_account))
      journal.errors.add(:to_account, :Haben_Konto,
        message: error_text)
    end
  end
end

class Journal < ApplicationRecord
  belongs_to :user
  
  validates :from_account_number,  presence: true, length: { maximum: 50 }
  validates :from_account,  presence: true
  validates :from_amount,  presence: true, length: { maximum: 50 }
  validates :to_account_number,  presence: true, length: { maximum: 50 }
  validates :to_account,  presence: true
  validates :to_amount,  presence: true, length: { maximum: 50 }
  
  validates_with KontoValidator
end

