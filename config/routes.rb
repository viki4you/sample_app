Rails.application.routes.draw do
  
  get 'welcomes/home'
  
  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  root to: 'welcomes#home'
  get 'users/new'
  get  '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :users do
    get 'journals/hauptbuch'
    get 'journals/konto'
    get 'journals/abschluss'
    get 'journals/createPdf'
    resources :journals 
  end
  resources :welcomes
  
  
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end