class AddFromAccountNumberToJournal < ActiveRecord::Migration[5.0]
  def change
    add_column :journals, :from_account_number, :string
  end
end
