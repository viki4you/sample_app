class AddFromAccountToJournal < ActiveRecord::Migration[5.0]
  def change
    add_column :journals, :from_account, :string
  end
end
