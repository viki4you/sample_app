class AddFromAmountToJournal < ActiveRecord::Migration[5.0]
  def change
    add_column :journals, :from_amount, :string
  end
end
