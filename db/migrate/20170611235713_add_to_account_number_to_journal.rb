class AddToAccountNumberToJournal < ActiveRecord::Migration[5.0]
  def change
    add_column :journals, :to_account_number, :string
  end
end
