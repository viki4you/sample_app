class AddToAccountToJournal < ActiveRecord::Migration[5.0]
  def change
    add_column :journals, :to_account, :string
  end
end
