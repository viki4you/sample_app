class AddToAmountToJournal < ActiveRecord::Migration[5.0]
  def change
    add_column :journals, :to_amount, :string
  end
end
