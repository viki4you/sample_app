class CreateIkrKontenplans < ActiveRecord::Migration[5.0]
  def change
    create_table :ikr_kontenplans do |t|
      t.string :nummer
      t.string :name

      t.timestamps
    end
  end
end
