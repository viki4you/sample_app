class AddBackupToJournals < ActiveRecord::Migration[5.0]
  def change
    add_column :journals, :backup, :boolean, default: false
  end
end
