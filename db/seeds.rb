# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#file = File.open("IKR_kontenplan_label_kontonummer.txt", "r+")
#file.close
require 'open-uri'

path = File.join Rails.root, 'public', 'try_out'
file = open(File.join(path, 'out.txt'))
contents = file.read
contents.each_line do |line|
  konto_nummer = line[0..(line.index(':') - 1)]
  konto_name = line[(line.index(':') + 2)..(line.length - 2)]
  IkrKontenplan.create!(:nummer => konto_nummer, :name => konto_name)
end