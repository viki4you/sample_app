[{
        label: "Ausstehende Einlagen",
        value: "0000"},
    {
        label: "Frei",
        value: "01"},
    {
        label: "Konzessionen",
        value: "0200"},
    {
        label: "Geschäfts- oder Firmenwert",
        value: "0300"},
    {
        label: "Frei",
        value: "04"},
    {
        label: "Unbebaute Grundstücke",
        value: "0500"},
    {
        label: "Bebaute Grundstücke",
        value: "0510"},
    {
        label: "Gebäude (Sammelkonto)",
        value: "0520"},
    {
        label: "Betriebsgebäude",
        value: "0530"},
    {
        label: "Verwaltungsgebäude",
        value: "0540"},
    {
        label: "Andere Bauten",
        value: "0550"},
    {
        label: "Grundstückseinrichtungen",
        value: "0560"},
    {
        label: "Gebäudeeinrichtungen",
        value: "0570"},
    {
        label: "Wohngebäude",
        value: "0590"},
    {
        label: "Frei",
        value: "06"},
    {
        label: "Anlagen und Maschinen der Energieversorgung",
        value: "0700"},
    {
        label: "Anlagen der Materiallagerung und -bereitstellung",
        value: "0710"},
    {
        label: "Anlagen und Maschinen der mechanischen Materialbearbeitung, -verarbeitung und -umwandlung",
        value: "0720"},
    {
        label: "Anlagen für Wärme-, Kälte und chemische Prozesse sowie ähnliche Anlagen",
        value: "0730"},
    {
        label: "Anlagen für Arbeitssicherheit und Umweltschutz",
        value: "0740"},
    {
        label: "Transportanlagen und ähnliche Betriebsvorrichtungen",
        value: "0750"},
    {
        label: "Verpackungsanlagen und -maschinen",
        value: "0760"},
    {
        label: "Sonstige Anlagen und Maschinen",
        value: "0770"},
    {
        label: "Reservemaschinen und -anlagenteile",
        value: "0780"},
    {
        label: "Geringwertige Anlagen und Maschinen",
        value: "0790"},
    {
        label: "GWG Sammelposten Anlagen und Maschinen Jahr 1",
        value: "0791"},
    {
        label: "GWG Sammelposten Anlagen und Maschinen Jahr 5",
        value: "0795"},
    {
        label: "Andere Anlagen, Betriebs- und Geschäftsausstattung",
        value: "08"},
    {
        label: "Andere Anlagen",
        value: "0800"},
    {
        label: "Werkstätteneinrichtung",
        value: "0810"},
    {
        label: "Werkzeuge, Werksgeräte und Modelle, Prüf- und Messmittel",
        value: "0820"},
    {
        label: "Lager- u. Transporteinrichtungen",
        value: "0830"},
    {
        label: "Fuhrpark",
        value: "0840"},
    {
        label: "Sonstige Betriebsausstattung",
        value: "0850"},
    {
        label: "Büromaschinen, Organisationsmit-tel und Kommunikationsanlagen",
        value: "0860"},
    {
        label: "Büromöbel und sonstige Geschäftsausstattung",
        value: "0870"},
    {
        label: "Reserveteile für Betriebs- und Geschäftsausstattung",
        value: "0880"},
    {
        label: "Geringwertige Vermögensgegenstände der Betriebsund Geschäftsausstattung",
        value: "0890"},
    {
        label: "GWG-Sammelposten BGA Jahr 1",
        value: "0891"},
    {
        label: "GWG-Sammelposten BGA Jahr 5",
        value: "0895"},
    {
        label: "Geleistete Anzahlungen und Anlagen im Bau",
        value: "09"},
    {
        label: "Geleistete Anzahlungen auf Sachanlagen",
        value: "0900"},
    {
        label: "Anlagen im Bau",
        value: "0950"
}]