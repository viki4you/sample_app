require 'test_helper'

class JournalsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @journalFirst = journals(:AktivAktiv)
    @journalSecond = journals(:PassivPassiv)
    @journalThird = journals(:AktivErfolg)
    log_in_as(@user)
  end
  
  test "hauptbuch responses" do
    get user_journals_hauptbuch_path(@user)
    assert_response :success
  end
  
  test "hauptbuch has unique entries" do
    book(@user, @journalFirst)
    book(@user, @journalFirst)
    
    get user_journals_hauptbuch_path(@user)
    
    assert_select "tr", 3
  end
  
  test "abschluss" do
    book(@user, @journalFirst)
    book(@user, @journalFirst)
    
    book(@user, @journalSecond)
    book(@user, @journalSecond)
    
    book(@user, @journalThird)
    book(@user, @journalThird)
    
    get user_journals_abschluss_path(@user)
    
    @user.journals.each do |row|
      ausgabe(row)
    end
  end
  
  test "abschluss twice" do
    book(@user, @journalFirst)
    book(@user, @journalFirst)
    
    book(@user, @journalSecond)
    book(@user, @journalSecond)
    
    book(@user, @journalThird)
    book(@user, @journalThird)
    
    get user_journals_abschluss_path(@user)
    get user_journals_abschluss_path(@user)
    
    @user.journals.each do |row|
      ausgabe(row)
    end
  end
end