ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Returns true if a test user is logged in.
  def is_logged_in?
    !session[:user_id].nil?
  end
  
  # Log in as a particular user.
  def log_in_as(user)
    session[:user_id] = user.id
  end
  
  def book(user, journal)
    post user_journals_path(user), params: { journal: { 
        user_id: journal.user_id,
        from_account_number: journal.from_account_number,
        from_account: journal.from_account,
        from_amount: journal.from_amount,
        to_account_number: journal.to_account_number,
        to_account: journal.to_account,
        to_amount: journal.to_amount
    }, umsatzsteuer: {
      soll_umsatzsteuer_id: "",
      haben_umsatzsteuer_id: ""
    }}
  end
  
  def ausgabe(row)
    puts row.user_id.to_s + ", " + row.id.to_s + ", " + 
          row.from_account_number + ", " + row.from_account +
          ", " + row.from_amount + ", " + row.to_account_number +
          ", " + row.to_account + ", " + row.to_amount +
          ", " + row.backup.to_s
  end
end

class ActionDispatch::IntegrationTest

  # Log in as a particular user.
  def log_in_as(user, password: 'password', remember_me: '0')
    post login_path, params: { session: { email: user.email,
                                          password: password,
                                          remember_me: remember_me } }
  end
end

# Test
@value_konto_nummer_kontenplan = [{
        label: "Ausstehende Einlagen",
        value: "0000"},
    {
        label: "Frei",
        value: "01"},
    {
        label: "Konzessionen",
        value: "0200"},
    {
        label: "Geschäfts- oder Firmenwert",
        value: "0300"},
    {
        label: "Frei",
        value: "04"},
    {
        label: "Unbebaute Grundstücke",
        value: "0500"},
    {
        label: "Bebaute Grundstücke",
        value: "0510"},
    {
        label: "Gebäude (Sammelkonto)",
        value: "0520"},
    {
        label: "Betriebsgebäude",
        value: "0530"},
    {
        label: "Verwaltungsgebäude",
        value: "0540"},
    {
        label: "Andere Bauten",
        value: "0550"},
    {
        label: "Grundstückseinrichtungen",
        value: "0560"},
    {
        label: "Gebäudeeinrichtungen",
        value: "0570"},
    {
        label: "Wohngebäude",
        value: "0590"},
    {
        label: "Frei",
        value: "06"},
    {
        label: "Anlagen und Maschinen der Energieversorgung",
        value: "0700"},
    {
        label: "Anlagen der Materiallagerung und -bereitstellung",
        value: "0710"},
    {
        label: "Anlagen und Maschinen der mechanischen Materialbearbeitung, -verarbeitung und -umwandlung",
        value: "0720"},
    {
        label: "Anlagen für Wärme-, Kälte und chemische Prozesse sowie ähnliche Anlagen",
        value: "0730"},
    {
        label: "Anlagen für Arbeitssicherheit und Umweltschutz",
        value: "0740"},
    {
        label: "Transportanlagen und ähnliche Betriebsvorrichtungen",
        value: "0750"},
    {
        label: "Verpackungsanlagen und -maschinen",
        value: "0760"},
    {
        label: "Sonstige Anlagen und Maschinen",
        value: "0770"},
    {
        label: "Reservemaschinen und -anlagenteile",
        value: "0780"},
    {
        label: "Geringwertige Anlagen und Maschinen",
        value: "0790"},
    {
        label: "GWG Sammelposten Anlagen und Maschinen Jahr 1",
        value: "0791"},
    {
        label: "GWG Sammelposten Anlagen und Maschinen Jahr 5",
        value: "0795"},
    {
        label: "Andere Anlagen, Betriebs- und Geschäftsausstattung",
        value: "08"},
    {
        label: "Andere Anlagen",
        value: "0800"},
    {
        label: "Werkstätteneinrichtung",
        value: "0810"},
    {
        label: "Werkzeuge, Werksgeräte und Modelle, Prüf- und Messmittel",
        value: "0820"},
    {
        label: "Lager- u. Transporteinrichtungen",
        value: "0830"},
    {
        label: "Fuhrpark",
        value: "0840"},
    {
        label: "Sonstige Betriebsausstattung",
        value: "0850"},
    {
        label: "Büromaschinen, Organisationsmit-tel und Kommunikationsanlagen",
        value: "0860"},
    {
        label: "Büromöbel und sonstige Geschäftsausstattung",
        value: "0870"},
    {
        label: "Reserveteile für Betriebs- und Geschäftsausstattung",
        value: "0880"},
    {
        label: "Geringwertige Vermögensgegenstände der Betriebsund Geschäftsausstattung",
        value: "0890"},
    {
        label: "GWG-Sammelposten BGA Jahr 1",
        value: "0891"},
    {
        label: "GWG-Sammelposten BGA Jahr 5",
        value: "0895"},
    {
        label: "Geleistete Anzahlungen und Anlagen im Bau",
        value: "09"},
    {
        label: "Geleistete Anzahlungen auf Sachanlagen",
        value: "0900"},
    {
        label: "Anlagen im Bau",
        value: "0950"},
    {
        label: "Frei",
        value: "10"},
    {
        label: "Frei",
        value: "11"},
    {
        label: "Frei",
        value: "12"},
    {
        label: "Beteiligungen",
        value: "13"},
    {
        label: "Beteiligungen",
        value: "1300"},
    {
        label: "Frei",
        value: "14"},
    {
        label: "Wertpapiere d. Anlagevermögens",
        value: "15"},
    {
        label: "Wertpapiere d. Anlagevermögens",
        value: "1500"},
    {
        label: "Sonstige Finanzanlagen",
        value: "16"},
    {
        label: "Sonstige Finanzanlagen",
        value: "1600"},
    {
        label: "Frei",
        value: "17"},
    {
        label: "Frei",
        value: "18"},
    {
        label: "Frei",
        value: "19"},
    {
        label: "Roh-, Hilfs- und Betriebsstoffe e",
        value: "20"},
    {
        label: "Rohstoffe/Fertigungsmaterial",
        value: "2000"},
    {
        label: "Bezugskosten",
        value: "2001"},
    {
        label: "Nachlässe",
        value: "2002"},
    {
        label: "Vorprodukte/Fremdbauteile",
        value: "2010"},
    {
        label: "Bezugskosten",
        value: "2011"},
    {
        label: "Nachlässe",
        value: "2012"},
    {
        label: "Hilfsstoffe",
        value: "2020"},
    {
        label: "Bezugskosten",
        value: "2021"},
    {
        label: "Nachlässe",
        value: "2022"},
    {
        label: "Betriebsstoffe",
        value: "2030"},
    {
        label: "Bezugskosten",
        value: "2031"},
    {
        label: "Nachlässe",
        value: "2032"},
    {
        label: "Sonstiges Material",
        value: "2070"},
    {
        label: "Bezugskosten",
        value: "2071"},
    {
        label: "Nachlässe",
        value: "2072"},
    {
        label: "Unfertige Erzeugnisse, unfertige Leistungen",
        value: "21"},
    {
        label: "Unfertige Erzeugnisse",
        value: "2100"},
    {
        label: "Unfertige Leistungen",
        value: "2190"},
    {
        label: "Fertige Erzeugnisse und Waren",
        value: "22"},
    {
        label: "Fertige Erzeugnisse",
        value: "2200"},
    {
        label: "Waren (Handelswaren) e",
        value: "2280"},
    {
        label: "Bezugskosten",
        value: "2281"},
    {
        label: "Nachlässe",
        value: "2282"},
    {
        label: "Geleistete Anzahlungen a. Vorräte",
        value: "23"},
    {
        label: "Geleistete Anzahlungen a. Vorräte",
        value: "2300"},
    {
        label: "Ford. a. Lieferungen u. Leistungen",
        value: "24"},
    {
        label: "Forderungen aus Lieferungen und Leistungen",
        value: "2400"},
    {
        label: "Kaufpreisforderungen",
        value: "2420"},
    {
        label: "Umsatzsteuerforderungen",
        value: "2421"},
    {
        label: "Wechselford. aus Lieferungen und Leistungen (Besitzwechsel)",
        value: "2450"},
    {
        label: "Zweifelhafte Forderungen",
        value: "2470"},
    {
        label: "Protestwechsel",
        value: "2480"},
    {
        label: "Innergemeinschaftlicher Erwerb/Einfuhr",
        value: "25"},
    {
        label: "Innergemeinschaftl. Erwerb",
        value: "2500"},
    {
        label: "Bezugskosten",
        value: "2501"},
    {
        label: "Nachlässe",
        value: "2502"},
    {
        label: "Gütereinfuhr",
        value: "2510"},
    {
        label: "Bezugskosten",
        value: "2511"},
    {
        label: "Nachlässe",
        value: "2512"},
    {
        label: "Sonstige Vermögensgegenstände",
        value: "26"},
    {
        label: "Vorsteuer",
        value: "2600"},
    {
        label: "Vorsteuer (19 %) für i. E.",
        value: "2602"},
    {
        label: "Einfuhrumsatzsteuer",
        value: "2604"},
    {
        label: "Sonst. Ford. an Finanzbehörden",
        value: "2630"},
    {
        label: "SV-Vorauszahlung",
        value: "2640"},
    {
        label: "Forderungen an Mitarbeiter",
        value: "2650"},
    {
        label: "Übrige sonstige Forderungen",
        value: "2690"},
    {
        label: "Wertpapiere des Umlaufvermögens",
        value: "27"},
    {
        label: "Wertpapiere des Umlaufvermögens",
        value: "2700"},
    {
        label: "Flüssige Mittel",
        value: "28"},
    {
        label: "Guthaben bei Kreditinstituten (Bank)",
        value: "2800"},
    {
        label: "Postbank",
        value: "2850"},
    {
        label: "Schecks",
        value: "2860"},
    {
        label: "Bundesbank",
        value: "2870"},
    {
        label: "Kasse",
        value: "2880"},
    {
        label: "Nebenkassen",
        value: "2890"},
    {
        label: "Aktive Rechnungsabgrenzung (und Bilanzfehlbetrag)",
        value: "29"},
    {
        label: "Aktive Jahresabgrenzung",
        value: "2900"},
    {
        label: "Umsatzsteuer auf erhaltene Anzahlungen",
        value: "2920"},
    {
        label: "Disagio",
        value: "2930"},
    {
        label: "(nicht durch Eigenkapital gedeckter Fehlbetrag)",
        value: "2990"},
    {
        label: "Eigenkapital/ Gezeichnetes Kapital",
        value: "30"},
    {
        label: "Eigenkapital",
        value: "3000"},
    {
        label: "Privatkonto Bei Personengesellschaften",
        value: "3001"},
    {
        label: "Kapital Gesellschafter A",
        value: "3000"},
    {
        label: "Privatkonto A",
        value: "3001"},
    {
        label: "Kapital Gesellschafter B",
        value: "3010"},
    {
        label: "Privatkonto B",
        value: "3011"},
    {
        label: "Kommanditkapital Gesellschafter C",
        value: "3070"},
    {
        label: "Kommanditkapital Gesellschafter D",
        value: "3080"},
    {
        label: "Gezeichnetes Kapital (Grundkapital/Stammkapital)",
        value: "3000"},
    {
        label: "Kapitalrücklage",
        value: "31"},
    {
        label: "Kapitalrücklage",
        value: "3100"},
    {
        label: "Gewinnrücklagen",
        value: "32"},
    {
        label: "Gesetzliche Rücklagen",
        value: "3210"},
    {
        label: "Satzungsmäßige Rücklagen",
        value: "3230"},
    {
        label: "Andere Gewinnrücklagen",
        value: "3240"},
    {
        label: "Ergebnisverwendung b",
        value: "33"},
    {
        label: "Jahresergebnis des Vorjahres",
        value: "3310"},
    {
        label: "Ergebnisvortrag aus früheren Perioden",
        value: "3320"},
    {
        label: "Veränderung der Rücklagen",
        value: "3340"},
    {
        label: "Bilanzgewinn/Bilanzverlust",
        value: "3350"},
    {
        label: "Ergebnisausschüttung",
        value: "3360"},
    {
        label: "Ergebnisvortrag auf neue Rechnung",
        value: "3390"},
    {
        label: "Jahresüberschuss/ Jahresfehlbetrag",
        value: "34"},
    {
        label: "Jahresüberschuss/ Jahresfehlbetrag",
        value: "3400"},
    {
        label: "Sonderposten mit Rücklageanteil",
        value: "35"},
    {
        label: "Sonderposten mit Rücklageanteil",
        value: "3500"},
    {
        label: "Wertberichtigungen (Bei Kapitalgesellschaften als Passivposten der Bilanz nicht mehr zulässig)",
        value: "36"},
    {
        label: "zu Sachanlagen",
        value: "3610"},
    {
        label: "zu Finanzanlagen",
        value: "3650"},
    {
        label: "Einzelwertberichtigung zu Forderungen",
        value: "3670"},
    {
        label: "Pauschalwertberichtigung zu Forderungen Rückstellungen",
        value: "3680"},
    {
        label: "Rückstellungen für Pensionen und ähnliche Verpflichtungen",
        value: "37"},
    {
        label: "Rückstellungen für Pensionen und ähnliche Verpflichtungen",
        value: "3700"},
    {
        label: "Steuerrückstellungen",
        value: "38"},
    {
        label: "Steuerrückstellungen",
        value: "3800"},
    {
        label: "Sonstige Rückstellungen c",
        value: "39"},
    {
        label: "für Gewährleistung",
        value: "3910"},
    {
        label: "für andere ungewisse Verbindlichkeiten",
        value: "3930"},
    {
        label: "für drohende Verluste aus schwebenden Geschäften",
        value: "3970"},
    {
        label: "für Aufwendungen",
        value: "3990"},
    {
        label: "Frei",
        value: "40"},
    {
        label: "Anleihen",
        value: "41"},
    {
        label: "Anleihen",
        value: "4100"},
    {
        label: "Verbindlichkeiten gegenüber Kreditinstituten",
        value: "42"},
    {
        label: "Kurzfristige Bankverbindlichkeiten",
        value: "4210"},
    {
        label: "Mittelfristige Bankverbindlichkeiten",
        value: "4230"},
    {
        label: "Langfristige Bankverbindlichkeiten",
        value: "4250"},
    {
        label: "Erhaltene Anzahlungen auf Bestellungen",
        value: "43"},
    {
        label: "Erhaltene Anzahlungen",
        value: "4300"},
    {
        label: "Verbindlichkeiten aus Lieferungen und Leistungen",
        value: "44"},
    {
        label: "Verbindlichkeiten aus Lieferungen und Leistungen",
        value: "4400"},
    {
        label: "Kaufpreisverbindlichkeiten",
        value: "4420"},
    {
        label: "Wechselverbindlichkeiten",
        value: "45"},
    {
        label: "Schuldwechsel",
        value: "4500"},
    {
        label: "Frei",
        value: "46"},
    {
        label: "Frei",
        value: "47"},
    {
        label: "Sonstige Verbindlichkeiten",
        value: "48"},
    {
        label: "Umsatzsteuer",
        value: "4800"},
    {
        label: "Umsatzsteuer (19 %) für i. E.",
        value: "4802"},
    {
        label: "Zollverbindlichkeiten",
        value: "4820"},
    {
        label: "Sonstige Verbindlichkeiten gegenüber Finanzbehörden",
        value: "4830"},
    {
        label: "Verbindlichkeiten gegenüber Mitarbeitern",
        value: "4850"},
    {
        label: "Verbindlichkeiten aus vermö- genswirksamen Leistungen",
        value: "4860"},
    {
        label: "Verbindlichkeiten gegenüber Gesellschaftern (Dividende)",
        value: "4870"},
    {
        label: "Übrige sonstige Verbindlichkeiten",
        value: "4890"},
    {
        label: "Passive Rechnungsabgrenzung",
        value: "49"},
    {
        label: "Passive Jahresabgrenzung",
        value: "4900"},
    {
        label: "Umsatzerlöse für eigene Erzeugnisse u. andere eigene Leistungen",
        value: "50"},
    {
        label: "Umsatzerlöse f. eigene Erzeugn.",
        value: "5000"},
    {
        label: "Erlösberichtigungen",
        value: "5001"},
    {
        label: "Umsatzerlöse für andere eigene Leistungen",
        value: "5050"},
    {
        label: "Erlösberichtigungen",
        value: "5051"},
    {
        label: "Erlöse aus innergemeinschaftlicher Lieferung (i. L.)",
        value: "5060"},
    {
        label: "Erlösberichtigungen",
        value: "5061"},
    {
        label: "Erlöse aus Güterausfuhr",
        value: "5070"},
    {
        label: "Erlösberichtigungen",
        value: "5071"},
    {
        label: "Umsatzerlöse für Waren und sonstige Umsatzerlöse",
        value: "51"},
    {
        label: "Umsatzerlöse für Waren",
        value: "5100"},
    {
        label: "Erlösberichtigungen",
        value: "5101"},
    {
        label: "Sonstige Umsatzerlöse",
        value: "5190"},
    {
        label: "Erlösberichtigungen",
        value: "5191"},
    {
        label: "Erhöhung oder Verminderung des Bestandes an unfertigen und fertigen Erzeugnissen",
        value: "52"},
    {
        label: "Bestandsveränderungen",
        value: "5200"},
    {
        label: "Bestandsveränderungen an unfertigen Erzeugnissen und nicht abgerechneten Leistungen",
        value: "5201"},
    {
        label: "Bestandsveränderungen an fertigen Erzeugnissen",
        value: "5202"},
    {
        label: "Andere aktivierte Eigenleistungen",
        value: "53"},
    {
        label: "Aktivierte Eigenleistungen",
        value: "5300"},
    {
        label: "Sonstige betriebliche Erträge",
        value: "54"},
    {
        label: "Mieterträge",
        value: "5400"},
    {
        label: "Leasingerträge",
        value: "5401"},
    {
        label: "Sonstige Erlöse (z. B. aus Provisionen oder Anlagenabgängen)",
        value: "5410"},
    {
        label: "Entnahme von Gegenständen und sonstigen Leistungen",
        value: "5420"},
    {
        label: "Andere sonstige betriebl. Erträge",
        value: "5430"},
    {
        label: "Erträge aus Werterhöhungen von Gegenständen des Anlage-vermögens(Zuschreibungen)",
        value: "5440"},
    {
        label: "Erträge aus Zuschreibungen zum Umlaufvermögen",
        value: "5441"},
    {
        label: "Erträge aus der Auflösung oder Herabsetzung von Wertberichtigungen auf Forderungen",
        value: "5450"},
    {
        label: "Erträge aus der Herabsetzung von Rückstellungen",
        value: "5480"},
    {
        label: "Periodenfremde Erträge",
        value: "5490"},
    {
        label: "Erträge aus Beteiligungen",
        value: "55"},
    {
        label: "Erträge aus Beteiligungen",
        value: "5500"},
    {
        label: "Erträge aus anderen Wertpapieren und Ausleihungen des Finanzanlagevermögens",
        value: "56"},
    {
        label: "Erträge aus anderen Finanzanlagen",
        value: "5600"},
    {
        label: "Sonstige Zinsen und ähnliche Erträge",
        value: "57"},
    {
        label: "Zinserträge",
        value: "5710"},
    {
        label: "Diskonterträge",
        value: "5730"},
    {
        label: "Erträge aus Wertpapieren des Umlaufvermögens",
        value: "5780"},
    {
        label: "Sonstige zinsähnliche Erträge",
        value: "5790"},
    {
        label: "Außerordentliche Erträge",
        value: "58"},
    {
        label: "Außerordentliche Erträge",
        value: "5800"},
    {
        label: "Frei",
        value: "59"},
    {
        label: "Aufwendungen für Roh-, Hilfs-und Betriebsstoffe und für bezogene Waren e",
        value: "60"},
    {
        label: "Aufwendungen für Rohstoffe/Fertigungsmaterial",
        value: "6000"},
    {
        label: "Bezugskosten",
        value: "6001"},
    {
        label: "Nachlässe",
        value: "6002"},
    {
        label: "Aufwendungen für Vorprodukte/Fremdbauteile e",
        value: "6010"},
    {
        label: "Aufwendungen für Hilfsstoffe e",
        value: "6020"},
    {
        label: "Aufwendungen für Betriebsstoffe/Verbrauchswerkzeuge e",
        value: "6030"},
    {
        label: "Aufw. für Verpackungsmaterial",
        value: "6040"},
    {
        label: "Aufw. für Energie u. Treibstoffe",
        value: "6050"},
    {
        label: "Aufw. für Reparaturmaterial",
        value: "6060"},
    {
        label: "Aufwendungen für sonstiges Material",
        value: "6070"},
    {
        label: "Aufwendungen für Waren e",
        value: "6080"},
    {
        label: "Aufwendungen für bezogene Leistungen",
        value: "61"},
    {
        label: "Fremdleistungen für Erzeugnisse und andere Umsatzleistungen",
        value: "6100"},
    {
        label: "Frachten und Fremdlager",
        value: "6140"},
    {
        label: "Vertriebsprovisionen",
        value: "6150"},
    {
        label: "Fremdinstandhaltung",
        value: "6160"},
    {
        label: "Sonstige Aufwendungen für bezogene Leistungen",
        value: "6170"},
    {
        label: "Löhne",
        value: "62"},
    {
        label: "Löhne einschl. tariflicher, vertraglicher oder arbeitsbedingter Zulagen",
        value: "6200"},
    {
        label: "Urlaubs- und Weihnachtsgeld",
        value: "6210"},
    {
        label: "Sonstige tarifliche oder vertragliche Aufwendungen für Lohnempfänger",
        value: "6220"},
    {
        label: "Freiwillige Zuwendungen",
        value: "6230"},
    {
        label: "Sachbezüge",
        value: "6250"},
    {
        label: "Vergütungen an gewerbliche Auszubildende",
        value: "6260"},
    {
        label: "Gehälter",
        value: "63"},
    {
        label: "Gehälter und Zulagen",
        value: "6300"},
    {
        label: "Urlaubs- und Weihnachtsgeld",
        value: "6310"},
    {
        label: "Sonstige tarifliche oder vertragliche Aufwendungen",
        value: "6320"},
    {
        label: "Freiwillige Zuwendungen",
        value: "6330"},
    {
        label: "Sachbezüge",
        value: "6350"},
    {
        label: "Vergütungen an Auszubildende",
        value: "6360"},
    {
        label: "Soziale Abgaben und Aufwendungen für Altersversorgung und für Unterstützung",
        value: "64"},
    {
        label: "Arbeitgeberanteil zur Sozialversicherung (Lohnbereich) d",
        value: "6400"},
    {
        label: "Arbeitgeberanteil zur Sozialversicherung (Gehaltsbereich) d",
        value: "6410"},
    {
        label: "Beiträge zur Berufsgenossenschaft",
        value: "6420"},
    {
        label: "Aufwendungen für Altersversorgung",
        value: "6440"},
    {
        label: "Aufwendungen für Unterstützung",
        value: "6490"},
    {
        label: "Sonstige soziale Aufwendungen",
        value: "6495"},
    {
        label: "Abschreibungen auf Anlagevermögen",
        value: "65"},
    {
        label: "Abschreibungen auf immaterielle Vermögensgegenstände des Anlagevermögens",
        value: "6510"},
    {
        label: "Abschreibungen auf Sachanlagen",
        value: "6520"},
    {
        label: "Abschreibungen auf geringwertige Wirtschaftsgüter",
        value: "6540"},
    {
        label: "Abschreibungen auf GWG-Sammelposten Jahr 1",
        value: "6541"},
    {
        label: "Abschreibungen auf GWG-Sammelposten Jahr 5",
        value: "6545"},
    {
        label: "Außerplanmäßige Abschreibungen auf Sachanlagen",
        value: "6550"},
    {
        label: "Unüblich hohe Abschreibungen auf Umlaufvermögen",
        value: "6570"},
    {
        label: "Sonstige Personalaufwendungen",
        value: "66"},
    {
        label: "Aufwendungen für Personaleinstellung",
        value: "6600"},
    {
        label: "Aufwendungen für übernommene Fahrtkosten",
        value: "6610"},
    {
        label: "Aufwendungen für Werksarzt und Arbeitssicherheit",
        value: "6620"},
    {
        label: "Personenbezogene Versicherungen",
        value: "6630"},
    {
        label: "Aufwendungen für Fort- und Weiterbildung",
        value: "6640"},
    {
        label: "Aufwendungen für Dienstjubiläen",
        value: "6650"},
    {
        label: "Aufwendungen für Belegschaftsveranstaltungen",
        value: "6660"},
    {
        label: "Aufwendungen für Werksküche und Sozialeinrichtungen",
        value: "6670"},
    {
        label: "Ausgleichsabgabe nach dem Schwerbehindertengesetz",
        value: "6680"},
    {
        label: "Übrige sonstige Personalaufwendungen",
        value: "6690"},
    {
        label: "Aufwendungen für die Inanspruchnahme von Rechten und Diensten",
        value: "67"},
    {
        label: "Mieten, Pachten",
        value: "6700"},
    {
        label: "Leasingaufwendungen",
        value: "6710"},
    {
        label: "Lizenzen und Konzessionen",
        value: "6720"},
    {
        label: "Gebühren",
        value: "6730"},
    {
        label: "Kosten des Geldverkehrs",
        value: "6750"},
    {
        label: "Provisionsaufwendungen (außer Vertriebsprovisionen)",
        value: "6760"},
    {
        label: "Rechts- und Beratungskosten",
        value: "6770"},
    {
        label: "Aufwendungen für Kommunikation(Dokumentation, Information, Reisen, Werbung)",
        value: "68"},
    {
        label: "Büromaterial",
        value: "6800"},
    {
        label: "Zeitungen und Fachliteratur",
        value: "6810"},
    {
        label: "Portokosten",
        value: "6820"},
    {
        label: "Kosten der Telekommunikation",
        value: "6830"},
    {
        label: "Reisekosten",
        value: "6850"},
    {
        label: "Bewirtung und Präsentation",
        value: "6860"},
    {
        label: "Werbung",
        value: "6870"},
    {
        label: "Spenden",
        value: "6880"},
    {
        label: "Aufwendungen für Beiträge und Sonstiges sowie Wertkorrekturen und periodenfremde Aufwendungen",
        value: "69"},
    {
        label: "Versicherungsbeiträge",
        value: "6900"},
    {
        label: "Beiträge zu Wirtschaftsverbänden und Berufsvertretungen",
        value: "6920"},
    {
        label: "Verluste aus Schadensfällen",
        value: "6930"},
    {
        label: "Sonstige Aufwendungen",
        value: "6940"},
    {
        label: "Abschreibungen auf Forderungen",
        value: "6950"},
    {
        label: "Abschreibungen auf Forderungen wegen Uneinbringlichkeit",
        value: "6951"},
    {
        label: "Einstellung in Einzelwertberichtigung",
        value: "6952"},
    {
        label: "Einstellung in Pauschalwertberichtigung",
        value: "6953"},
    {
        label: "Verluste aus dem Abgang von Vermögensgegenständen",
        value: "6960"},
    {
        label: "Anlagenabgänge",
        value: "6979"},
    {
        label: "Zuführungen zu Rückstellungen für Gewährleistung",
        value: "6980"},
    {
        label: "Periodenfremde Aufwendungen",
        value: "6990"},
    {
        label: "Betriebliche Steuern",
        value: "70"},
    {
        label: "Grundsteuer",
        value: "7020"},
    {
        label: "Grundsteuer – Vorjahr",
        value: "7021"},
    {
        label: "Kraftfahrzeugsteuer",
        value: "7030"},
    {
        label: "Kraftfahrzeugsteuer – Vorjahr",
        value: "7031"},
    {
        label: "Steuerrückerstattung für KfzSteuer – Vorjahr",
        value: "7032"},
    {
        label: "Ausfuhrzölle",
        value: "7070"},
    {
        label: "Verbrauchsteuern",
        value: "7080"},
    {
        label: "Sonstige betriebliche Steuern",
        value: "7090"},
    {
        label: "Frei",
        value: "71"},
    {
        label: "Frei",
        value: "72"},
    {
        label: "Frei",
        value: "73"},
    {
        label: "Abschreibungen auf Finanzanlagen und auf Wertpapiere des Umlaufvermögens und Verluste aus entsprechenden Abgängen",
        value: "74"},
    {
        label: "Abschreibungen auf Finanzanlagen",
        value: "7400"},
    {
        label: "Abschreibungen auf Wertpapiere des Umlaufvermögens",
        value: "7420"},
    {
        label: "Verluste aus dem Abgang von Finanzanlagen",
        value: "7450"},
    {
        label: "Verluste aus dem Abgang von Wertpapieren des Umlaufvermögens",
        value: "7460"},
    {
        label: "Zinsen und ähnliche Aufwendungen",
        value: "75"},
    {
        label: "Zinsaufwendungen",
        value: "7510"},
    {
        label: "Diskontaufwendungen",
        value: "7530"},
    {
        label: "Sonstige zinsähnliche Aufwendungen",
        value: "7590"},
    {
        label: "Außerordentliche Aufwendungen",
        value: "76"},
    {
        label: "Außerordentliche Aufwendungen",
        value: "7600"},
    {
        label: "Steuern vom Einkommen und Ertrag",
        value: "77"},
    {
        label: "Gewerbesteuer",
        value: "7700"},
    {
        label: "Gewerbesteuer – Vorjahr",
        value: "7701"},
    {
        label: "Steuerrückerstattung für Gewerbesteuer – Vorjahr",
        value: "7702"},
    {
        label: "Körperschaftsteuer",
        value: "7710"},
    {
        label: "Kapitalertragsteuer",
        value: "7720"},
    {
        label: "Diverse Aufwendungen",
        value: "78"},
    {
        label: "Diverse Aufwendungen",
        value: "7800"},
    {
        label: "Frei",
        value: "79"},
    {
        label: "Eröffnung/Abschluss",
        value: "80"},
    {
        label: "Eröffnungsbilanzkonto",
        value: "8000"},
    {
        label: "Schlussbilanzkonto",
        value: "8010"},
    {
        label: "GuV-Konto Gesamtkostenverfahren",
        value: "8020"},
    {
        label: "GuV-Konto Umsatzkostenverfahren",
        value: "8030"},
    {
        label: "Saldenvorträge(Sammelkonto)Konten der Kostenbereiche für die GuV im Umsatzkostenverfahren",
        value: "8050"},
    {
        label: "Herstellungskosten",
        value: "81"},
    {
        label: "Vertriebskosten",
        value: "82"},
    {
        label: "Allgemeine Verwaltungskosten",
        value: "83"},
    {
        label: "Sonstige betriebliche Aufwendungen Konten der kurzfristigen Erfolgsrechnung (KER) für innerjährige Rechnungsperioden (Monat, Quartal oder Halbjahr)",
        value: "84"},
    {
        label: "Korrekturkonten zu den Erträgen der Kontenklasse 5",
        value: "85"},
    {
        label: "Korrekturkonten zu den Aufwendungen der Kontenklasse 6",
        value: "86"},
    {
        label: "Korrekturkonten zu den Aufwendungen der Kontenklasse 7",
        value: "87"},
    {
        label: "Kurzfristige Erfolgsrechnung (KER)",
        value: "88"},
    {
        label: "Gesamtkostenverfahren",
        value: "8800"},
    {
        label: "Umsatzkostenverfahren",
        value: "8810"},
    {
        label: "Innerjährige Rechnungsabgrenzung",
        value: "89"},
    {
        label: "Aktive Rechnungsabgrenzung",
        value: "8900"},
    {
        label: "Unternehmensbezogene Abgrenzungen (neutrale Aufwendungen u. Erträge)",
        value: "90"},
    {
        label: "Kostenrechnerische Korrekturen",
        value: "91"},
    {
        label: "Kostenarten und Leistungsarten",
        value: "92"},
    {
        label: "Kostenstellen",
        value: "93"},
    {
        label: "Kostenträger",
        value: "94"},
    {
        label: "Fertige Erzeugnisse",
        value: "95"},
    {
        label: "Interne Lieferungen und Leistungen sowie deren Kosten",
        value: "96"},
    {
        label: "Umsatzkosten",
        value: "97"},
    {
        label: "Umsatzleistungen",
        value: "98"},
    {
        label: "Ergebnisausweise",
        value: "99"}];